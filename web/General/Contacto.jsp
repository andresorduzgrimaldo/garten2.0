

<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            SigfejaFacade fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (!fachada.existeNegocioGeneral()) {
                response.sendRedirect("../cerrarSesion.jsp");
        %>

        <%
        } else {
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Garten</title>
    </head>
    <body>
        <jsp:include page="../public/includes/header.jsp" />
        <!-- *** TOP BAR END *** -->

        <!-- *** NAVBAR ***
     _________________________________________________________ -->

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <!--/.navbar-header -->

                <jsp:include page="../public/includes/menupublico.jsp" />
                <!--/.nav-collapse -->

                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="index.jsp">Inicio</a></li>
                            <li>Contacto</li>
                        </ul>

                    </div>

                    <div class="col-md-12">
                        <div id="results" class="box">
                            <h1 class="text-primary">Contacto</h1>

                            
                            <hr>

                            <div class="row">
                                
                                <!-- /.col-sm-4 -->
                              
                                <!-- /.col-sm-4 -->
                                <div class="col-sm-4">
                                    <h3><i class="fa fa-envelope"></i> Correo Electrónico</h3>
                                    <p class="text-muted">Tienes alguna duda, queja o sugerencia nos puedes escribir a nuestro correo porque estamos para servirle</p>
                                    <strong><a href="mailto:">soportegarten2.0@gmail.com</a></strong>

                                </div>
                                <!-- /.col-sm-4 -->
                                <div class="col-sm-4">
                                        <img src="<%= request.getContextPath()%>/public/img/logo.jpg" class="img img-responsive" alt="">
                                    
                                </div>
                                <div class="col-sm-4">
                                        <img src="<%= request.getContextPath()%>/public/img/logogidis.bmp" class="img img-responsive" alt="">
                                    
                                </div>
                                
                            </div>
                            <!-- /.row -->

                            <hr>

                            
                        </div>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->

                <jsp:include page="../public/includes/footer.jsp" />
            </div>
            <!-- /#all -->
        </div>

        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap-hover-dropdown.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

        <script>
            function initialize() {
                var mapOptions = {
                    zoom: 15,
                    center: new google.maps.LatLng(7.898862,-72.4874143),
                    mapTypeId: google.maps.MapTypeId.ROAD,
                    scrollwheel: false
                }
                var map = new google.maps.Map(document.getElementById('map'),
                        mapOptions);

                var myLatLng = new google.maps.LatLng(7.898862,-72.4874143);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </body>
    <%
        }
    %>
</html>
