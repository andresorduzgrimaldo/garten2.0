
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <%
            SigfejaFacade fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (!fachada.existeNegocioGeneral()) {
                response.sendRedirect("../cerrarSesion.jsp");
        %>

        <%
        } else {
        %>
        <title>Garten</title>

        <jsp:include page="../public/includes/importarlibrerias.jsp" />
    </head>

    <body>

        <!-- *** TOPBAR ***
     _________________________________________________________ -->

        <jsp:include page="../public/includes/header.jsp" />
        <!-- *** TOP BAR END *** -->

        <!-- *** NAVBAR ***
     _________________________________________________________ -->

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <!--/.navbar-header -->

                <jsp:include page="../public/includes/menupublico.jsp" />
                <!--/.nav-collapse -->

                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#navbar -->

        <!-- *** NAVBAR END *** -->

        <div id="all">

            <div id="content">

                <div class="container">
                    <div class="col-md-12">
                        <div id="main-slider">
                            <div class="item">
                                <img src="../public/img/slider-1.jpg" alt="" class="img-responsive">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="../public/img/slider-2.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="../public/img/slider-3.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="../public/img/slider-4.jpg" alt="">
                            </div>
                        </div>
                        <!-- /#main-slider -->
                    </div>
                </div>

     
                <div id="advantages">

                    <div class="container">
                        <div class="same-height-row">
                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon"><i class="fa fa-tags"></i>
                                    </div>

                                    <h3><a href="#">Mejores productos</a></h3>
                                    <p>te ofrecemos los mejores servicios del mercado.</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon"><i class="fa fa-heart"></i>
                                    </div>

                                    <h3><a href="#">Nuestra comunidad</a></h3>
                                    <p>haz parte de nuestra comunidad y obten muchos  beneficios al ser parte de esta gran familia.</p>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="box same-height clickable">
                                    <div class="icon"><i class="fa fa-thumbs-up"></i>
                                    </div>

                                    <h3><a href="#">Servicios</a></h3>
                                    <p>Conoce nuestros servicios y obten el mejor aprovechamiento de tus recursos.</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.container -->

                </div>
                <!-- /#advantages -->

              

            </div>
            <!-- /#content -->

            <jsp:include page="../public/includes/footer.jsp" />

            <!-- /#content -->
        </div>
        <!-- /#all -->
        <!-- *** SCRIPTS TO INCLUDE ***
     _________________________________________________________ -->
        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/jquery.cookie.js"></script>
        <script src="../public/js/waypoints.min.js"></script>
        <script src="..//public/js/modernizr.js"></script>
        <script src="../public/js/bootstrap/bootstrap-hover-dropdown.js"></script>
        <script src="../public/js/owl.carousel.min.js"></script>
        <script src="../public/js/front.js"></script>
    </body>
    <%
        }
    %>
</html>