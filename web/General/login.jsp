
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <title>Garten</title>

        <jsp:include page="../public/includes/importarlibrerias.jsp" />
    </head>

    <body>

        <!-- *** TOPBAR ***
     _________________________________________________________ -->

        <jsp:include page="../public/includes/header.jsp" />
        <!-- *** TOP BAR END *** -->

        <!-- *** NAVBAR ***
     _________________________________________________________ -->

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <!--/.navbar-header -->

                <jsp:include page="../public/includes/menupublico.jsp" />
                <!--/.nav-collapse -->

                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#navbar -->

        <!-- *** NAVBAR END *** -->

        <div id="all">

            <div class="container">

                <div class="col-md-12">

                    <ul class="breadcrumb">
                        <li><a href="index.jsp">Home</a>
                        </li>
                        <li>Acceder</li>
                    </ul>

                </div>

                <div class="col-md-6">
                    <div class="box">
                        <h1>Iniciar Sesion</h1>

                        <p class="lead"></p>
                        <p class="text-muted">Ingresa y empieza a disfrutar de todos los beneficios que te ofrece Garten</p>

                        <hr>

                        <form action="<%=request.getContextPath() %>/LoginServlet" method="post">
                            <div class="form-group">
                                <label for="email">Cédula</label>
                                <input type="text" class="form-control" name="cedula" id="cedula">
                            </div>
                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <div class="text-center">
                                <div id="divCargando"></div>
                                <button id="btnAcceder" type="submit" class="btn btn-primary"><i class="fa fa-sign-in"></i> Acceder</button>
                            </div>
                        </form>
                            
                            
                    </div>
                </div>
                            
                                  <div class="col-md-6">
                    <div class="box">
                        <h1>Registrarse</h1>

                        <p class="lead"></p>
                        <p class="text-muted">Aun no haces parte de nuestra comunidad?</p>
                        <p class="text-muted">Registrate y empieza a disfrutar de los beneficios que te ofrece Garten.</p>

                        <hr>

                       <form id="formRegistro" action="registrarCliente" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Nombre</label>
                                                    <input type="text" class="form-control" id="Nombre" name="Nombre" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Apellido</label>
                                                    <input type="text" class="form-control" id="Apellido" name="Apellido" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Cédula</label>
                                                    <input type="text" class="form-control" id="Cedula" name="Cedula" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Teléfono</label>
                                                    <input type="text" class="form-control" id="Telefono" name="Telefono" required>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control" id="Correo" name="Correo" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Dirección</label>
                                                    <input type="text" class="form-control" name="Direccion" id="Direccion" required/>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">



                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password">Contraseña</label>
                                                    <input type="password" class="form-control" name="contrasena" id="contrasena" required/>
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="password">Confirmar Contraseña</label>
                                                    <input type="password" class="form-control" name="contrasena2" id="contrasena2" required/>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12 text-center">
                                                <button  id="btnRegistroCliente" class="btn btn-primary" type="submit"><i class="fa fa-user"></i>Registrar</button>
                                                
                                            </div>
                                        </div>
                                    </form>

                            
                    </div>
                </div>
            </div>
            <!-- /#content -->

            <jsp:include page="../public/includes/footer.jsp" />

            <!-- /#content -->
        </div>
        <!-- /#all -->
        <!-- *** SCRIPTS TO INCLUDE ***
     _________________________________________________________ -->
        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/jquery.cookie.js"></script>
        <script src="../public/js/waypoints.min.js"></script>
        <script src="..//public/js/modernizr.js"></script>
        <script src="../public/js/bootstrap/bootstrap-hover-dropdown.js"></script>
        <script src="../public/js/owl.carousel.min.js"></script>
        <script src="../public/js/front.js"></script>
        <script src="../public/js/Personas.js" type="text/javascript"></script>
        <script src="../public/js/General.js" type="text/javascript"></script>
    </body>
</html>