
<div id="footer" data-animate="fadeInUp">
    <div class="container">
        <div class="row">
           
            <!-- /.col-md-3 -->

            <div class="col-md-4 col-sm-6">

                
                <h4>Productos</h4>

                <ul>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=1&tipo=1&pagina=1">Plantas</a>
                    </li>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=2&tipo=2&pagina=1">Herramientas</a>
                    </li>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=3&tipo=3&pagina=1">Accesorios y Semillas</a>
                    </li>
                </ul>

                      <hr class="hidden-md hidden-lg">
             
            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-4 col-sm-6">

                
              <h4>Servicios</h4>
                <ul>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=1&tipo=1&pagina=1">Ayuda con mi jardin</a>
                    </li>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=1&tipo=2&pagina=1">Consulta especializada</a>
                    </li>
                    <li><a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=1&tipo=3&pagina=1">Alquiler de terreno</a>
                    </li>
                </ul>
                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->



            <div class="col-md-4 col-sm-6">

                <h4>S�guenos</h4>

                <p class="social">
                    <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                </p>
                
               

            </div>
            <!-- /.col-md-3 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>
<!-- /#footer -->

<!-- *** FOOTER END *** -->




<!-- *** COPYRIGHT ***
_________________________________________________________ -->
<div id="copyright">
    <div class="container">
        <div class="col-md-6">
            <p class="pull-left">� 2017 Garten.</p>

        </div>
        <div class="col-md-6">
            <p class="pull-right">UFPS - <a href="http://ingsistemas.ufps.edu.co/">Ingenier�a de Sistemas</a>  
            </p>
        </div>
    </div>
</div>
<!-- *** COPYRIGHT END *** -->