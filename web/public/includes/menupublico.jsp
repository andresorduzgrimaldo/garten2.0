<div class="navbar-header">

    <a class="navbar-brand home" href="<%= request.getContextPath()%>/" data-animate-hover="bounce">
        <img src="<%= request.getContextPath()%>/public/img/log.png" alt="Garten logo" class="hidden-xs">
        <img src="<%= request.getContextPath()%>/public/img/log-small.png" alt="Garten logo" class="visible-xs"><span class="sr-only">Obaju - go to homepage</span>
    </a>

</div>
<div class="navbar-collapse collapse" id="navigation">

    <ul class="nav navbar-nav navbar-left">
        <li class="active"><a href="<%= request.getContextPath()%>/">Inicio</a>
        </li>
        <li class="dropdown yamm-fw">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Productos <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <div class="yamm-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <h5>Plantas</h5>
                                <div class="banner">
                                    <a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=1&tipo=1&pagina=1">
                                        <img src="<%= request.getContextPath()%>/public/img/plantas.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5>Herramientas</h5>
                                <div class="banner">
                                    <a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=2&tipo=2&pagina=1">
                                        <img src="<%= request.getContextPath()%>/public/img/herramientas.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5>Semillas/Accesorios</h5>
                                <div class="banner">
                                    <a href="<%= request.getContextPath()%>/General/Categoria.jsp?cat=3&tipo=3&pagina=1">
                                        <img src="<%= request.getContextPath()%>/public/img/semillas.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="banner">
                                    <a href="#">
                                        <img src="<%= request.getContextPath()%>/public/img/logo.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.yamm-content -->
                </li>
            </ul>
        </li>

        <li class="dropdown yamm-fw">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Servicios <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <div class="yamm-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <h5>Ayuda con mi jardin</h5>
                                <div class="banner">
                                    <a href="login.jsp">
                                        <img src="<%= request.getContextPath()%>/public/img/ayuda.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5>Consulta especializada</h5>
                                <div class="banner">
                                    <a href="login.jsp">
                                        
                                        <img src="<%= request.getContextPath()%>/public/img/consulta.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5>Alquiler de terreno</h5>
                                <div class="banner">
                                    <a href="login.jsp">
                                        <img src="<%= request.getContextPath()%>/public/img/terreno.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="banner">
                                    <a href="#">
                                        <img src="<%= request.getContextPath()%>/public/img/logo.jpg" class="img img-responsive" alt="">
                                    </a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <!-- /.yamm-content -->
                </li>
            </ul>
        </li>
      </ul>
</div>
                                   
<!--/.nav-collapse -->