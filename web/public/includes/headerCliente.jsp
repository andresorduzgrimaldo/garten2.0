
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<div id="top">
    <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getClienteN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }
        %>
        <%
            try {

                Cliente cli= Fachada.getClienteN().getCliente();
        %>
    
            <div class="container">
                <div class="col-md-6 offer" data-animate="fadeInDown">
                      <a href="#">Bienvenido <%= cli.getNombre()%> <%= cli.getApellido()%> disfruta de la mejor experiencia </a>
                </div>
                <div class="col-md-6" data-animate="fadeInDown">
                    <ul class="menu">
                        <li><a href="Profile.jsp">Perfil</a></li>
                        <li><a href="carritoDeCompras.jsp"><i class="fa fa-shopping-cart"></i></a></li>
                        <li><a href="Contacto.jsp">Contáctanos</a></li>
                        <li><a id="btncerrarSession" href="../cerrarSesion.jsp"><i class="fa fa-sign-out"></i> Cerrar Sesion</a></li>
                    </ul>
                </div>
            </div>
 <%                                            } catch (Exception e) {
        e.printStackTrace();
    %>
    <script>
            location = "../General/index.jsp";
    </script>
    <%
        }
    %>
        </div>