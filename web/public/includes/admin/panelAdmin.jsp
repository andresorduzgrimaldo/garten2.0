<div class="col-md-3">
    <!-- *** CUSTOMER MENU ***
_________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">

        <div class="panel-heading">
            <h3 class="panel-title">Men� Administrador</h3>
        </div>

        <div class="panel-body">

            <ul id="listadeMenu" class="nav nav-pills nav-stacked">
                <li id="li1">
                    <a href="ModificarDatos.jsp" id="modificardatos"><i class="fa fa-credit-card"></i>Mis datos</a>
                </li>
                
                <li id="li2">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil"></i>Gestion de clientes</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="registrarCliente.jsp">Registrar Clientes</a></li>
                        <li><a href="listarClientes.jsp">Listado de Clientes</a></li>
                        <li class="divider"></li>
                        <li><a href="reactivarClientes.jsp">Reactivar Clientes</a></li>
                    </ul>
                </li>
               
                <li id="li3">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Gestion de Empleados</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="registrarEmpleado.jsp">Registrar empleados</a></li>
                        <li><a href="listarEmpleados.jsp">Listado de empleados</a></li>
                        <li class="divider"></li>
                        <li><a href="reactivarEmpleados.jsp">Reactivar empleados</a></li>
                    </ul>
                    
                </li>
               
                <li id="li4">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-users"></i>Gestion de proveedores</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="registrarProveedor.jsp">Registrar proveedor</a></li>
                        <li><a href="listarProveedores.jsp">Listado de proveedores</a></li>
                        <li class="divider"></li>
                        <li><a href="reactivarProveedores.jsp">Reactivar proveedor</a></li>
                    </ul>
                </li>
                 
                
              
                <li id="li5">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-archive"></i>Gestionar Productos</a>
                    <ul class="dropdown-menu" role="menu">
                         <li><a href="registrarProducto.jsp">Registrar Productos</a></li>
                        <li><a href="subirProducto.jsp">Subir Productos</a></li>
                        <li><a href="listarProductos.jsp">Listado de Productos</a></li>
                    </ul>
                </li>
                <li id="li6">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-clock-o"></i>Gestion de Citas</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="registroCita.jsp">Registrar Citas</a></li>
                        <li><a href="listarCitas.jsp">Listado de Citas</a></li>
                    </ul>
                </li>
               
               <li id="li7">
                    <a href="listarPedidos.jsp"><i class="fa fa-shopping-cart"></i>Pedidos</a>
                </li> 
                    
                <li id="li8">
                    <a id="btncerrarSession" href="../cerrarSesion.jsp"><i class="fa fa-sign-out"></i> Salir</a>
                </li>
            </ul>
        </div>

    </div>
    <!-- /.col-md-3 -->

    <!-- *** CUSTOMER MENU END *** -->
</div>