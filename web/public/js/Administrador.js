
//Métodos relacionados solo con el Admin
$('#btnModificarDatos').on('click', function (e) {
    e.preventDefault();
    modificarDatos();
});

$('#btnCambiarContrasena').on('click', function (e) {
    e.preventDefault();
    cambiarContrasena();
});

function modificarDatos() {

    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Direccion = $('#Direccion').val();
    var Telefono = $('#Telefono').val();
    var Correo = $('#Correo').val();

if(Nombre===""||Apellido===""||Telefono===""||Correo===""||Direccion===""){
    alert("Campos Vacios");
}else{

    $.ajax({
        type: "POST",
        url: "../Admin",
        data: {metodo: "modificarDatos", Nombre: Nombre, Apellido: Apellido, Direccion: Direccion, Telefono: Telefono, Correo: Correo}
    }).done(function (respuesta) {

        alert(respuesta);

        if (respuesta === "Datos Actualizados") {
            location.reload();
        }
    });
}
}

function cambiarContrasena() {

    var Contrasena = $('#contrasena').val();
    var Contrasenamod = $('#contrasenamod').val();
    var Contrasenamod2 = $('#contrasenamod2').val();

    if (Contrasenamod === Contrasenamod2) {
        
        $.ajax({
            type: "POST",
            url: "../Admin",
            data: {metodo: "cambiarContraseña", Contrasena: Contrasena, ContrasenaNueva: Contrasenamod}
        }).done(function (respuesta) {

            alert(respuesta);
            if (respuesta === "Contraseña Actualizada") {
                location.reload();
            }
            if(respuesta==="Error contraseña actual incorrecta"){
                location.reload();
            }
        });
    } else {
        alert("Las Contraseñas Nuevas no Coinciden");
    }
}

//Métodos Relacionados con la Gestión de Clientes

$('#btnRegistroCliente').on('click', function (e) {
    e.preventDefault();
    registrarCliente();
});

$('[name="btnActualizar"]').on('click', function (e) {
    e.preventDefault();
    mostrarDatosCliente(this.id);
});

$('[name="btnDesactivar"]').on('click', function (e) {
    e.preventDefault();
    desactivarCliente(this.id);
});

$('[name="btnReactivar"]').on('click', function (e) {
    e.preventDefault();
    reactivarCliente(this.id);
});

$('[name="btnConfirmarPedido"]').on('click', function (e) {
    e.preventDefault();
    confirmarPedido(this.id);
});


function mostrarDatosCliente(id) {

    ocultarDiv(2);

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "getCliente", Cedula: id
        }
    }).done(function (respuesta) {
        $('#divModificar').html(respuesta);
    });
}

function guardarDatosCliente() {

    var cedula = $('[name="btnModificarDatosCliente"]').attr("id");
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var correo = $('#correo').val();

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "modificarCliente", cedula:cedula, nombre:nombre, apellido:apellido,direccion:direccion,telefono:telefono,correo:correo
        }
    }).done(function (respuesta) {
        alert(respuesta);
        location.reload();
    });
}

function reactivarCliente(id) {

    var confirmacion = confirm("¿Está seguro de que desea realizar la reactivacion?");

    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "reactivar", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}

function reactivarCliente(id) {

    var confirmacion = confirm("¿Está seguro de que desea realizar la reactivacion?");

    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "reactivar", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}


function confirmarPedido(id) {

    var confirmacion = confirm("¿Desea confirmar la compra?");
    if (confirmacion === true) {
       
        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "confirmarPedido", codigoP: id
                
            }
             
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
        
    }
}


function desactivarCliente(id) {

    var confirmacion = confirm("¿Estas seguro de que desea realizar la desactivación?");
    
    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "desactivar", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}



function registrarCliente() {

    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Cedula = $('#Cedula').val();
    var Telefono = $('#Telefono').val();
    var Correo = $('#Correo').val();
    var Direccion = $('#Direccion').val();
    var Contrasena = $('#contrasena').val();
    var Contrasena2 = $('#contrasena2').val();
    
if(Nombre===""||Apellido===""||Cedula===""||Telefono===""||Correo===""||Direccion===""||Contrasena===""||Contrasena2===""){
    alert("Campos Vacios");
}else{
    if (Contrasena === Contrasena2) {

        $.ajax({
            type: "POST",
            url: "../Admin",
            data: {metodo: "registrarCliente", Nombre: Nombre, Apellido: Apellido, Cedula: Cedula, Direccion: Direccion,
                Telefono: Telefono, Correo: Correo, Contrasena: Contrasena}
        }).done(function (respuesta) {

            if (respuesta === 1062) {
                respuesta = "El Usuario Ya Se Encuentra Registrado";
                location.reload();
            }
            alert(respuesta);

            if (respuesta === "Cliente Registrado") {
                location.reload();
            }
        });
    } else {
        alert("Las Contraseñas No Coinciden");
    }
}
}



//Gestion de proveedores

$('#btnRegistroProveedor').on('click', function (e) {
    e.preventDefault();
    registrarProveedor();
});

$('[name="btnActualizarProveedor"]').on('click', function (e) {
    e.preventDefault();
    mostrarDatosProveedor(this.id);
});

$('[name="btnDesactivarProveedor"]').on('click', function (e) {
    e.preventDefault();
    desactivarProveedor(this.id);
});

$('[name="btnReactivarProveedor"]').on('click', function (e) {
    e.preventDefault();
    reactivarProveedor(this.id);
});


function mostrarDatosProveedor(id) {

    ocultarDiv(2);

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "getProveedor", Cedula: id
        }
    }).done(function (respuesta) {
        $('#divModificar').html(respuesta);
    });
}

function guardarDatosProveedor() {

    var cedula = $('[name="btnModificarDatosProveedor"]').attr("id");
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var correo = $('#correo').val();

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "modificarProveedor", cedula:cedula, nombre:nombre, apellido:apellido,direccion:direccion,telefono:telefono,correo:correo
        }
    }).done(function (respuesta) {
        alert(respuesta);
        location.reload();
    });
}

function reactivarProveedor(id) {

    var confirmacion = confirm("¿Está seguro de que desea reactivar el proveedor seleccionado?");

    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "reactivarProveedor", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}

function desactivarProveedor(id) {

    var confirmacion = confirm("¿Está seguro de que desea desactivar proveedor seleccionado?");
    
    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "desactivarProveedor", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}

function registrarProveedor() {

    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Cedula = $('#Cedula').val();
    var Telefono = $('#Telefono').val();
    var Correo = $('#Correo').val();
    var Direccion = $('#Direccion').val();
    var id_tipo_producto=$('#id_tipo_producto').val();
    var Contrasena = $('#contrasena').val();
    var Contrasena2 = $('#contrasena2').val();
    
    if(Nombre===""||Apellido===""||Cedula===""||Telefono===""||Correo===""||Direccion===""||Contrasena===""||Contrasena2===""){
    alert("Campos Vacios");
}else{
    if (Contrasena === Contrasena2) {

        $.ajax({
            type: "POST",
            url: "../Admin",
            data: {metodo: "registrarProveedor", Nombre: Nombre, Apellido: Apellido, Cedula: Cedula, Direccion: Direccion, id_tipo_producto: id_tipo_producto,
                Telefono: Telefono, Correo: Correo, Contrasena: Contrasena}
        }).done(function (respuesta) {

            if (respuesta === 1062) {
                respuesta = "El Usuario Ya Se Encuentra Registrado";
                location.reload();
            }
            alert(respuesta);

            if (respuesta === "Proveedor Registrado") {
                location.reload();
            }
        });
    } else {
        alert("Las Contraseñas No Coinciden");
    }
}
}

function ocultarDiv(num) {

    if (num === 1) {
        $('#divListado').show();
        $('#divModificar').hide();
    }
    if (num === 2) {
        $('#divListado').hide();
        $('#divModificar').show();
    }
    }


//Gestion de empleados


$('#btnRegistroEmpleado').on('click', function (e) {
    e.preventDefault();
    registrarEmpleado();
});


$('[name="btnActualizarEmpleado"]').on('click', function (e) {
    e.preventDefault();
    mostrarDatosEmpleado(this.id);
});

$('[name="btnDesactivarEmpleado"]').on('click', function (e) {
    e.preventDefault();
    desactivarEmpleado(this.id);
});

$('[name="btnReactivarEmpleado"]').on('click', function (e) {
    e.preventDefault();
    reactivarEmpleado(this.id);
});


function mostrarDatosEmpleado(id) {

    ocultarDiv(2);

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "getEmpleado", Cedula: id
        }
    }).done(function (respuesta) {
        $('#divModificar').html(respuesta);
    });
}

function guardarDatosEmpleado() {

    var cedula = $('[name="btnModificarDatosEmpleado"]').attr("id");
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var correo = $('#correo').val();

    $.ajax({
        url: "../Admin",
        type: "POST",
        data: {
            metodo: "modificarEmpleado", cedula:cedula, nombre:nombre, apellido:apellido,direccion:direccion,telefono:telefono,correo:correo
        }
    }).done(function (respuesta) {
        alert(respuesta);
        location.reload();
    });
}

function reactivarProveedor(id) {

    var confirmacion = confirm("¿Está seguro de que desea reactivar el proveedor seleccionado?");

    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "reactivarProveedor", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}

function desactivarProveedor(id) {

    var confirmacion = confirm("¿Está seguro de que desea desactivar proveedor seleccionado?");
    
    if (confirmacion === true) {

        $.ajax({
            url: "../Admin",
            type: "POST",
            data: {
                metodo: "desactivarProveedor", cedula: id
            }
        }).done(function (respuesta) {
            alert(respuesta);
            location.reload();
        });
    }
}



function registrarEmpleado() {

    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Cedula = $('#Cedula').val();
    var Telefono = $('#Telefono').val();
    var Correo = $('#Correo').val();
    var Direccion = $('#Direccion').val();
    var cod_especialidad=$('#cod_especialidad').val();
    var Contrasena = $('#contrasena').val();
    var Contrasena2 = $('#contrasena2').val();
    
    if(Nombre===""||Apellido===""||Cedula===""||Telefono===""||Correo===""||Direccion===""||Contrasena===""||Contrasena2===""){
    alert("Campos Vacios");
}else{
    if (Contrasena === Contrasena2) {

        $.ajax({
            type: "POST",
            url: "../Admin",
            data: {metodo: "registrarEmpleado", Nombre: Nombre, Apellido: Apellido, Cedula: Cedula, Direccion: Direccion, 
                Telefono: Telefono, Correo: Correo, Contrasena: Contrasena,cod_especialidad:cod_especialidad}
        }).done(function (respuesta) {

            if (respuesta === 1062) {
                respuesta = "El Usuario Ya Se Encuentra Registrado";
                location.reload();
            }
            alert(respuesta);

            if (respuesta === "Empleado Registrado") {
                location.reload();
            }
        });
    } else {
        alert("Las Contraseñas No Coinciden");
    }
    }
}


//Gestion Producto

$('#btnRegistroProducto').on('click', function (e) {
    e.preventDefault();
    registrarProducto();
});


function registrarProducto() {

    var Nombre = $('#Nombre').val();
    var Valor = $('#Valor').val();
    var Cantidad = $('#Cantidad').val();
    var Categoria = $('#Categoria').val();
    var Tipo = $('#Tipo').val();
    var Descripcion = $('#Descripcion').val();
    
    
  if(Nombre===""||Valor===""||Cantidad===""||Categoria===""||Tipo===""||Descripcion===""){
    alert("Campos Vacios");
}else{

        $.ajax({
            type: "POST",
            url: "../Admin",
            data: {metodo: "registrarProducto", Nombre: Nombre, Valor: Valor, Cantidad: Cantidad, Tipo: Tipo, 
                Categoria: Categoria, Descripcion: Descripcion}
        }).done(function (respuesta) {

            if (respuesta === 1062) {
                respuesta = "El Producto Ya Se Encuentra Registrado";
                location.reload();
            }
            alert(respuesta);

            if (respuesta === "Producto Registrado") {
                location.reload();
            }
        });
    }
    }


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
