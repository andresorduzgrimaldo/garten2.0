
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<jsp:useBean class="co.edu.ufps.Sigfeja.facade.SigfejaFacade" id="Fachada" scope="session"></jsp:useBean>

<%
    long codigo_p = Long.parseLong(request.getParameter("Codigo"));
    String nombre = request.getParameter("Nombre");
    int valor = Integer.parseInt(request.getParameter("Valor"));
    int cantidad = Integer.parseInt(request.getParameter("Cantidad"));
    int idtipo = Integer.parseInt(request.getParameter("Tipo"));
    int idcategoria = Integer.parseInt(request.getParameter("Categoria"));
    String descripcion = request.getParameter("Descripcion");

    Categoria categoria = new Categoria();
    categoria.setId(idcategoria);
    Tipo tipo = new Tipo();
    tipo.setId(idtipo);
    
    Producto p = new Producto(codigo_p, nombre, descripcion, valor, cantidad, categoria, tipo, null);
    

    if (Fachada.modificarProducto(p)) {
%>
<script>
    alert("Producto Modificado Correctamente");
    location="listarProductos.jsp";
</script>
<%
} else {
%>
<script>
    alert("No se pudo modificar el producto");
    location="listarProductos.jsp";
</script>
<%
}
%>
