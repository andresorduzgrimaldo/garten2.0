
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.SQLException"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Registro Productos</title>
    </head>
    <body>
        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getAdminN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }

        %>

        <%            try {
                List<Categoria> categorias = Fachada.getCategorias();
                ArrayList<Tipo> tipos = Fachada.getTipos();
        %>

        <jsp:include page="../public/includes/admin/header.jsp" />

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="Profile.sp">Inicio</a></li>
                            <li>Mi Cuenta</li>
                        </ul>

                    </div>

                    <jsp:include page="../public/includes/admin/panelAdmin.jsp" />
                    <div>

                        <div>
                            <div class="col-md-9">
                                <div id="results" class="box">

                                    <h1 class="text-primary" align='center'>Registro de productos</h1>


                                    <hr>

                                    <form id="formRegistro" action="registrarProducto" method="post">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Nombre</label>
                                                    <input type="text" class="form-control" id="Nombre" name="Nombre" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Valor</label>
                                                    <input type="number" class="form-control" id="Valor" name="Valor" required>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Cantidad</label>
                                                    <input type="number" class="form-control" id="Cantidad" name="Cantidad" required>
                                                </div>

                                            </div>

                                            <div class="col-xs-6 form-group">
                                                <label for="text">Tipo de Prenda</label>
                                                <select name="Tipo" id="Tipo" class="form-control">
                                                    <%
                                                        for (Tipo tipo : tipos) {

                                                    %>
                                                    <option value="<%=tipo.getId()%>" selected><%=tipo.getDescripcion()%></option>
                                                    <%
                                                        }

                                                    %>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-md-6 form-group">
                                                <label for="text">Categoría</label>
                                                <select name="Categoria" id="Categoria"  class="form-control">
                                                    <%                                                    for (Categoria categoria : categorias) {
                                                    %>
                                                    <option value="<%=categoria.getId()%>" selected><%=categoria.getNombre()%></option>
                                                    <%
                                                        }

                                                    %>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 form-group">
                                            <label for="text">Descripción</label>
                                            <blockquote>
                                                <textarea class="form-control" rows="5" name="Descripcion" id="Descripcion"></textarea>
                                            </blockquote>
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-sm-12 text-center">
                                                <button  id="btnRegistroProducto" class="btn btn-success" type="submit"><i class="fa fa-user"></i>Registrar</button>
                                                <button class="btnCancelar btn btn-default" type="reset">Cancelar</button>

                                            </div>
                                        </div>


                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <!-- /.container -->
            </div>
            <!-- /#content -->

            <jsp:include page="../public/includes/footerLogin.jsp" />
        </div>
        <!-- /#all -->
    </div>

    <script src="../public/js/jquery-1.11.0.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
    <script src="../public/js/Administrador.js" type="text/javascript"></script>
</body>

<% } catch (SQLException e) {
%>
<%="Error SQL: " + e.getMessage()%>
<%
        e.printStackTrace();
    }
%>
</html>
