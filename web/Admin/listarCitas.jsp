

<%@page import="java.sql.SQLException"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Cita"%>
<%@page import="java.util.List"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getAdminN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }
            try {

                List<Cita> citas = Fachada.getAdminN().getListadoDeCitasPorEstado(1);
                List<Cita> citasApartadas = Fachada.getAdminN().getListadoDeCitasPorEstado(2);
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Administracion de citas</title>
    </head>
    <body>
        <jsp:include page="../public/includes/admin/header.jsp" />

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="#">Campañas</a>
                            </li>
                            <li>Listado</li>
                        </ul>

                    </div>

                    <jsp:include page="../public/includes/admin/panelAdmin.jsp" />
                    <div class="col-md-9">
                        <div id="results" class="box">
                            <h1 class="text-primary">Gestión Citas</h1>

                            <h3 class="text-center text-aqua lead">Citas Disponibles</h3>

                            <%
                                if (citas.isEmpty()) {
                            %>
                            <h4 class="text-center text-info">NO HAY CITAS DISPONIBLES</h4>
                            <%
                            } else {
                            %>

                            <br>
                            
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th class="text-center">Codigo cita</th>
                                            <th class="text-center">Fecha cita</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Empleado</th> 

                                        </tr>
                                    </thead>


                                    <%
                                        for (Cita cita : citas) {
                                    %>
                                    <tr>
                                        <td class="text-center"><%= cita.getId_cita()%></td>
                                        <td class="text-center"><%= cita.getFechaCitaString()%></td>
                                        <th class="text-center"><%= cita.getHora() %></th>
                                        <td class="text-center"><%= cita.getCedula_empleado().getCedula()%></td>
                              
                                    </tr>
                                    <%
                                        }
                                    %>
                                    </tbody>
                                </table>
                            </div>
                            <%
                                }
                            %>

                            <h3 class="text-center text-aqua lead">Citas Apartadas</h3>

                            <%
                                if (citasApartadas.isEmpty()) {
                            %>
                            <h4 class="text-center text-info">NO HAY CITAS APARTADAS</h4>
                            <%
                            } else {
                            %>

                            <br>

                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th class="text-center">Codigo cita</th>
                                            <th class="text-center">Fecha cita</th>
                                            <th class="text-center">Empleado</th> 
                                            <th class="text-center">Cliente</th> 

                                        </tr>
                                    </thead>

                                    <%
                                        for (Cita cita : citasApartadas) {
                                    %>
                                    <tr>
                                        <td class="text-center"><%=cita.getId_cita()%></td>
                                        <td class="text-center"><%=cita.getFecha_cita()%></td>
                                        <td class="text-center"><%=cita.getCedula_empleado().getCedula()%></td>
                                        <td class="text-center">en proceso</td>
                                    </tr>
                                    <%
                                        }
                                    %>


                                    </tbody>
                                </table>
                            </div>
                            <%
                                }
                            %>


                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <!-- /#content -->

            <jsp:include page="../public/includes/footerLogin.jsp" />
        </div>
        <!-- /#all -->
    </div>

    <script src="../public/js/jquery-1.11.0.min.js"></script>
    <script src="../public/js/bootstrap/bootstrap.min.js"></script>
</body>
<%
} catch (SQLException e) {
%>
<%="Error SQL: " + e.getMessage()%>
<%
        e.printStackTrace();
    }
%>
</html>
