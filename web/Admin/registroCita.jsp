

<%@page import="java.util.List"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado"%>
<%@page import="java.sql.SQLException"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getAdminN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }
            try {
                List<Empleado> e = Fachada.getAdminN().getEmpleados();
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <link href="../public/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>
        <link href="../public/css/dateTimePicker/bootstrap-datetimepicker-standalone.css" rel="stylesheet" type="text/css"/>
        <title>Administracion de Citas</title>
    </head>
    <body>
        <jsp:include page="../public/includes/admin/header.jsp" />

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="Profile.sp">Inicio</a></li>

                            <li>Citas</li>
                        </ul>

                    </div>

                    <jsp:include page="../public/includes/admin/panelAdmin.jsp" />

                    <div class="col-md-9">
                        <div id="results" class="box">



                            <h1 class="text-primary text-center">Gestión de Citas</h1>


                            <form id="registroCita" action="registrarCita.jsp" method="post">

                                <div class="row">
                                    <div class="col-md-4 form-group">
                                        <label for="text">Fecha de Cita</label>
                                        <input type="text" class="form-control" id="datepicker" name="Fecha_cita" required>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label>Hora Cita.</label>
                                        <input type='time' class="form-control" name='hora'>
                         

                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="especialidad">Empleado</label>
                                        <select name="cedula_empl" id="cedula_empl" class="form-control">
                                            <%
                                                for (Empleado em : e) {


                                            %>
                                            <option value="<%=em.getCedula()%>"><%=em.getCedula()%> / <%=em.getNombre()%> / <%=em.getCod_especialidad().getDescripcion()%></option>
                                            <%
                                                }
                                            %>
                                        </select>    
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-sm-12 form-group" align="center">
                                        <input class="btn btn-primary" type="submit" value="Registrar Cita" />
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->

                <jsp:include page="../public/includes/footerLogin.jsp" />
            </div>
            <!-- /#all -->
        </div>

        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap-hover-dropdown.js"></script>
        <script src="../public/js/bootstrap/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="../public/js/bootstrap/bootstrap-datepicker.es.min.js" type="text/javascript"></script>
        <script src="../public/js/moment.js" type="text/javascript"></script>
        <script src="../public/js/DateTimePicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script>

            var f = new Date();
            $('#registroCita #datepicker').datepicker({
                format: "yyyy-mm-dd",
                startDate: "" + f.getFullYear(),
                language: "es"
            });

            $('#datetimepicker').datetimepicker();
        </script>
    </body>
    <%
    } catch (Exception e) {
    %>
    <%="Error: " + e.getMessage()%>
    <%
            e.printStackTrace();
        }
    %>
</html>