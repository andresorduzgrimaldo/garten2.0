

<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (!Fachada.existeNegocioCliente()) {
        %>
        <script>
            alert("Acceso solo para Gerentes");
            location = "../cerrarSesion.jsp";
        </script>
        <%
            }
        %>
        <%
            try {

                Cliente cliente = Fachada.getClienteLogeado();
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Modificar Datos</title>
    </head>
    <body>
        <jsp:include page="../public/includes/headerCliente1.jsp" />

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <!--/.navbar-header -->

                <jsp:include page="../public/includes/menupublico.jsp" />
                <!--/.nav-collapse -->

                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="#">Inicio</a>
                            </li>
                            <li>Mi Cuenta</li>
                        </ul>

                    </div>

                    <jsp:include page="../public/includes/cliente/panelCliente.jsp" />
                    <div>
                        <div class="col-md-9">
                            <div id="results" class="box">

                                <h1>Modificar mis datos</h1>

                                <h3>Cambiar Contraseña</h3>

                                <form id="formCambiarContrasena" action="cambiarContrasena" method="post">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_old">Contraseña Actual</label>
                                                <input type="password" class="form-control" id="contrasena" name="contrasena" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_1">Nueva Contraseña</label>
                                                <input type="password" class="form-control" id="contrasenamod" name="contrasenamod" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_2">Repite la Nueva Contraseña</label>
                                                <input type="password" class="form-control" id="contrasenamod2" name="contrasenamod2" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 text-center">
                                            <button id="btnCambiarContrasena" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar Nueva Contraseña</button>
                                        </div>
                                    </div>
                                    <!-- /.row -->


                                </form>
                                <hr>
                                <form id="formActualizarDatos" action="update" method="post">
                                    <h3>Datos Personales</h3>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname">Nombres</label>
                                                <input type="text" class="form-control" id="Nombre" name="Nombre" value="<%=cliente.getNombre()%>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="lastname">Apellidos</label>
                                                <input type="text" class="form-control" id="Apellido" name="Apellido" value="<%=cliente.getApellido()%>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="street">Dirección</label>
                                                <input type="text" class="form-control" name="Direccion" id="Direccion" value="<%=cliente.getDireccion()%>" required />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Teléfono</label>
                                                <input type="number" class="form-control" name="Telefono" id="Telefono" value="<%=cliente.getTelefono()%>" required />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Correo</label>
                                                <input type="email" name="Correo" id="Correo" value="<%= cliente.getCorreo()%>" class="form-control" required title="Correo No Válido" />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <button id="btnModificarDatos" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Guardar Cambios</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->

                <jsp:include page="../public/includes/footerCliente.jsp" />
            </div>
            <!-- /#all -->
        </div>

        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/Cliente.js" type="text/javascript"></script>
    </body>
    <%                                            } catch (Exception e) {
        e.printStackTrace();
    %>
    <script>
            location = "../General/index.jsp";
    </script>
    <%
        }
    %>
</html>
