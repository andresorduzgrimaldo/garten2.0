/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Administrador;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cita;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Pedido;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public interface IAdminNegocio {

    //Datos del Admin
    public boolean login(Persona p) throws SQLException;

    public Administrador getAdmin();

    public void setAdmin(Administrador admin);

    public List<Categoria> getCategorias() throws SQLException;

    public Campaña getCampañaActiva();

    public boolean actualizarDatos(Administrador admin) throws SQLException;

    public boolean cambiarPassword(String contrasena, String contrasenanueva) throws SQLException;

    public List<Cliente> getListadoDeGerentes() throws SQLException;

    public List<Proveedor> getListadoDeProveedoresPorEstado(int estado) throws SQLException;

    public List<Cliente> getListadoDeClientesPorEstado(int estado) throws SQLException;

    public List<Empleado> getListadoDeEmpleadosPorEstado(int estado) throws SQLException;

    public boolean registrarCliente(Cliente gerente) throws SQLException;

    public List<Cita> getListadoDeCitasPorEstado(int est) throws SQLException, ParseException;

    public boolean registrarProveedor(Proveedor pro) throws SQLException;

    public boolean registrarEmpleado(Empleado em) throws SQLException;
    
    
    public boolean registrarProducto(Producto p) throws SQLException;

    public Cliente getCliente(String numDocumentoCliente) throws SQLException;

    public Empleado getEmpleado(String numDocumentoEmpleado) throws SQLException;

    public Proveedor getProveedor(String numDocumentoProveedor) throws SQLException;

    public boolean actualizarGerente(Cliente gerente) throws SQLException;

    public boolean actualizarEmpleado(Empleado emp) throws SQLException;

    public boolean cambiarEstadoCliente(String cedula, int estado) throws SQLException;
    
    public boolean confirmarPedido(long codigoP, int estado) throws SQLException;

    public Producto getProducto(long codigo_p);

    public ArrayList<Tipo> getTipos() throws SQLException;

    public ArrayList<Empleado> getEmpleados() throws SQLException;

    public ArrayList<Especialidad> getEspecialidades() throws SQLException;

    public boolean modificarProducto(Producto p) throws SQLException;

    public boolean insertarImagenDeProducto(ArrayList<String> urls, long codigo_p) throws SQLException;

    public List<Pedido> getPedidos() throws SQLException, ParseException;

   
    public boolean subirProductos(ArrayList<Producto> productos) throws SQLException;

    public boolean iniciarCampaña(Campaña campaña) throws SQLException, ParseException;

    public boolean iniciarCita(Cita cita) throws SQLException, ParseException;
}
