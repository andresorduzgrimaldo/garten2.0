/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDAO.CampañaDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.CategoriasDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.TipoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ClienteDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ProductoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class GeneralNegocio implements Serializable, IGeneralNegocio {

    private Campaña campañaActiva;
    private ProductoDAO productos;

    public GeneralNegocio() throws SQLException, ParseException {
        List<Campaña> campañas = new CampañaDAO().listarCampañasPorEstado(1);
        this.campañaActiva = (!campañas.isEmpty()) ? campañas.get(0) : null;
    }

    @Override
    public boolean login(Persona p) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     @Override
    public Categoria getCategoria(int id) throws SQLException {
        return new CategoriasDAO().getCategoria(id);
    }

     @Override
    public List<Categoria> getCategorias() throws SQLException {
        return new CategoriasDAO().getCategorias();
    }
    
    @Override
    public Campaña getCampañaActiva() {
        return campañaActiva;
    }

    public void setCampañaActiva(Campaña campañaActiva) {
        this.campañaActiva = campañaActiva;
    }
    
    
     @Override
    public List<Producto> getProductosCampañaPorCategoriaYTipo(int cat, int tipo) throws SQLException {

        List<Producto> productosL = this.campañaActiva.getProductos();
        List<Producto> productosPorCategoriaYTipo = new ArrayList();
        for (Producto producto : productosL) {
            
            if (producto.getCategoria().getId() == cat && producto.getTipoProducto().getId() == tipo) {
                productosPorCategoriaYTipo.add(producto);
            }
        }

        return productosPorCategoriaYTipo;

    }
     @Override
    public List<Producto> lista(int tipo) throws SQLException {

        List<Producto> productosL = this.productos.listar();
        List<Producto> productosPorCategoriaYTipo = new ArrayList();
        for (Producto producto : productosL) {
            
            if (producto.getTipoProducto().getId() == tipo) {
                productosPorCategoriaYTipo.add(producto);
            }
        }

        return productosPorCategoriaYTipo;

    }

    
    @Override
    public ArrayList<Tipo> getTipoProducto() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    @Override
    public boolean registrarCliente(Cliente gerente) throws SQLException {

        ClienteDAO gDAO = new ClienteDAO();

        return gDAO.insertar(gerente);

    }

    
    
    @Override
    public Tipo getTipo(int id) throws SQLException {
        return new TipoDAO().getTipo(id);
    }
    
    @Override
    public Producto getProducto(long codig_p) {

        for (Producto producto : this.campañaActiva.getProductos()) {
            if (producto.getCodigo_p() == codig_p) {
                return producto;
            }
        }
        return null;
    }
}
