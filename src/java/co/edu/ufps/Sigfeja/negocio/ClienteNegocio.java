/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDAO.CampañaDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.CategoriasDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ClienteDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOCliente;
import co.edu.ufps.Sigfeja.models.ClasesDAO.PedidoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ProductoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Item;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Pedido;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.util.Encriptador;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class ClienteNegocio implements Serializable, IClienteNegocio {

    private Cliente cliente;
    private Campaña campañaActiva;

    public ClienteNegocio(Persona p) {
        this.cliente = new Cliente();
        this.cliente.setCedula(p.getCedula());
        this.cliente.setNombre(p.getNombre());
        this.cliente.setApellido(p.getApellido());
        this.cliente.setCorreo(p.getCorreo());
        this.cliente.setTipoUsr(2);
        this.cliente.setDireccion(p.getDireccion());
        this.cliente.setValido(true);
        this.cliente.setTelefono(p.getTelefono());
        this.cliente.setContraseña(p.getContraseña());
        
        try {
            List<Campaña> campañas = new CampañaDAO().listarCampañasPorEstado(1);
            this.campañaActiva = (!campañas.isEmpty()) ? campañas.get(0) : null;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public ClienteNegocio(Cliente gerente, Campaña campañaActiva) {
        this.cliente = gerente;
        this.campañaActiva = campañaActiva;
    }

    
    
    public ClienteNegocio(String cedula) throws SQLException , ParseException {

        this.cliente = new ClienteDAO().getCliente(cedula);
        List<Campaña> campañas = new CampañaDAO().listarCampañasPorEstado(1);
        this.campañaActiva = (!campañas.isEmpty()) ? campañas.get(0) : null;
    
    }

    @Override
    public Producto getProducto(long codigo_p) {

        for (Producto producto : this.campañaActiva.getProductos()) {
            if (producto.getCodigo_p() == codigo_p) {
                return producto;
            }
        }
        return null;
    }
    
    @Override
    public Cliente getCliente() {
        return cliente;
    }

    @Override
    public void setGerente(Cliente gerente) {
        this.cliente = gerente;
    }

    @Override
    public Campaña getCampañaActiva() {
        return campañaActiva;
    }

     @Override
    public List<Pedido> getPedidosC(String cedula) throws SQLException, ParseException {
        return new PedidoDAO().getPedidoDelCliente(cedula);
    }
    
    @Override
    public Categoria getCategoriaC(int id) throws SQLException {
        return new CategoriasDAO().getCategoria(id);
    }
    
    @Override
    public boolean existeItem(long codigo_p) {
        boolean estado = false;

        if (this.cliente.getPedido() == null) {
            estado = false;
        } else {
            for (Item item : this.cliente.getPedido().getItems()) {
                if (item.getProducto().getCodigo_p() == codigo_p) {
                    estado = true;
                    break;
                }
            }
        }

        return estado;
    }


    
    @Override
    public boolean registrarPedido() throws SQLException {
        boolean estado = new PedidoDAO().insertar(this.cliente.getPedido(), this.campañaActiva.getCodigo_cam());

        if (estado) {
            this.cliente.getPedido().setEstado((byte) 1);
            this.cliente.getPedido().getValorTotal();
            Producto p = null;
            for(Item item:this.cliente.getPedido().getItems()){
                p = item.getProducto();
                new ProductoDAO().descontarUnidades(p.getCodigo_p(), item.getCantidad());
            }
           
        }
        return estado;
    }

    
    @Override
    public boolean agregarItemAlPedido(Item item) {
        Producto p= new Producto();
        Pedido pedido = null;
        
        
        try {
            
            if (this.cliente.getPedido() == null) {
                pedido = new Pedido(this.cliente.getCedula(), 0, null);
                pedido.setItems(new ArrayList<Item>());
            
            }else {
                pedido = this.cliente.getPedido();
            }
            
            pedido.agregarItem(item);
            pedido.setValorTotal(pedido.getValorTotal() + item.getValorTotal());
            this.cliente.setPedido(pedido);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        
        return true;
        
    }

    
     @Override
    public boolean eliminarItemDelPedido(int posicion_item) {

        Pedido pedido = this.cliente.getPedido();
        pedido.setValorTotal(pedido.getValorTotal()-pedido.getItems().get(posicion_item).getValorTotal());
        pedido.getItems().remove(posicion_item);
        return true;
    }
    
    @Override
    public List<Producto> getProductosCampañaPorCategoriaYTipoC(int cat, int tipo) throws SQLException {

        List<Producto> productosL = this.campañaActiva.getProductos();
        List<Producto> productosPorCategoriaYTipo = new ArrayList();
        for (Producto producto : productosL) {
            
            if (producto.getCategoria().getId() == cat && producto.getTipoProducto().getId() == tipo) {
                productosPorCategoriaYTipo.add(producto);
            }
        }

        return productosPorCategoriaYTipo;

    }
    
    @Override
    public boolean actualizarDatos(Cliente cliente) throws SQLException {

        IDAOCliente gDAO = new ClienteDAO();
        return gDAO.modificar(cliente);
    }

    @Override
    public boolean cambiarPassword(String contrasena, String contrasenanueva) throws SQLException {

        Encriptador e = new Encriptador();
        if (this.cliente.getContraseña().equals(e.encriptar(contrasena))) {
            IDAOCliente aDAO = new ClienteDAO();
            this.cliente.setContraseña(e.encriptar(contrasenanueva));

            return aDAO.cambiarContrasena(cliente);
        } else {
            return false;
        }

    }

}
