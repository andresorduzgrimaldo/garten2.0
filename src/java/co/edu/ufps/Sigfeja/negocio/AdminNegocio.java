package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDAO.AdministradorDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.CampañaDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.CategoriasDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.CitaDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ClienteDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.EmpleadoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.EspecialidadDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ImagenProductoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOAdministrador;
import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOCampana;
import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOCita;
import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOImagenProducto;
import co.edu.ufps.Sigfeja.models.ClasesDAO.PedidoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.PersonaDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ProductoDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ProveedorDAO;
import co.edu.ufps.Sigfeja.models.ClasesDAO.TipoDAO;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Administrador;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cita;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import co.edu.ufps.Sigfeja.models.ClasesDTO.ImagenProductoDTO;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Pedido;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import co.edu.ufps.Sigfeja.models.util.Encriptador;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class AdminNegocio implements Serializable, IAdminNegocio {

    private Administrador admin;
    private Campaña campañaActiva;

    public AdminNegocio() {
     }

    @Override
    public Campaña getCampañaActiva() {
        return campañaActiva;
    }

    public void setCampañaActiva(Campaña campañaActiva) {
        this.campañaActiva = campañaActiva;
    }

     @Override
    public List<Categoria> getCategorias() throws SQLException {
        return new CategoriasDAO().getCategorias();
    }
    
    public AdminNegocio(Persona p) {

        this.admin = new Administrador();
        this.admin.setCedula(p.getCedula());
        this.admin.setNombre(p.getNombre());
        this.admin.setApellido(p.getApellido());
        this.admin.setCorreo(p.getCorreo());
        this.admin.setTipoUsr(1);
        this.admin.setDireccion(p.getDireccion());
        this.admin.setValido(true);
        this.admin.setTelefono(p.getTelefono());
        this.admin.setContraseña(p.getContraseña());

        try {
            List<Campaña> campañas = new CampañaDAO().listarCampañasPorEstado(1);
            this.campañaActiva = (!campañas.isEmpty()) ? campañas.get(0) : null;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        
    }

    @Override
    public Administrador getAdmin() {
        return admin;
    }

    @Override
    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

    @Override
    public boolean login(Persona p) throws SQLException {

        Persona pe = new PersonaDAO().login(p);

        if (pe.getTipoUsr() == 1) {
            this.admin = (Administrador) pe;
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean actualizarDatos(Administrador admin) throws SQLException {

        AdministradorDAO aDAO = new AdministradorDAO();
        this.admin = admin;
        return aDAO.modificarDatos(admin);
    }

    @Override
    public boolean cambiarPassword(String contrasena, String contrasenanueva) throws SQLException {

        Encriptador e = new Encriptador();
        if (this.admin.getContraseña().equals(contrasena)) {
            IDAOAdministrador aDAO = new AdministradorDAO();
            this.admin.setContraseña(e.encriptar(contrasenanueva));

            return aDAO.cambiarContrasena(admin);
        } else {
            return false;
        }

    }

    @Override
    public List<Cliente> getListadoDeGerentes() throws SQLException {

        ClienteDAO gDAO = new ClienteDAO();

        return gDAO.listar();

    }

    @Override
    public List<Cliente> getListadoDeClientesPorEstado(int estado) throws SQLException {

        ClienteDAO gDAO = new ClienteDAO();

        return gDAO.listarPorEstado(estado);
    }
    
     @Override
    public List<Cita> getListadoDeCitasPorEstado(int est) throws SQLException, ParseException {

        IDAOCita cDAO = new CitaDAO();

        return cDAO.listarCitasPorEstado(est);
    }
    
     @Override
    public List<Proveedor> getListadoDeProveedoresPorEstado(int estado) throws SQLException {

        ProveedorDAO pDAO = new ProveedorDAO();

        return pDAO.listarPorEstado(estado);
    }
    
    @Override
    public boolean registrarCliente(Cliente gerente) throws SQLException {

        ClienteDAO gDAO = new ClienteDAO();

        return gDAO.insertar(gerente);

    }
    
   
    
    
    @Override
    public boolean registrarProveedor(Proveedor proveedor) throws SQLException {

        ProveedorDAO pDAO = new ProveedorDAO();

        return pDAO.insertar(proveedor);

    }
    
    @Override
    public boolean registrarEmpleado(Empleado empl) throws SQLException {

        EmpleadoDAO eDAO = new EmpleadoDAO();

        return eDAO.insertarEmp(empl);

    }
    
    @Override
    public boolean registrarProducto(Producto p ) throws SQLException {

        ProductoDAO pDAO = new ProductoDAO();

        return pDAO.insertar(p, this.campañaActiva);

    }

    @Override
    public List<Empleado> getListadoDeEmpleadosPorEstado(int estado) throws SQLException {

        EmpleadoDAO gDAO = new EmpleadoDAO();

        return gDAO.listarPorEstado(estado);
    }
    
    
    @Override
    public Cliente getCliente(String numDocumentoCliente) throws SQLException {

        ClienteDAO gDAO = new ClienteDAO();

        Cliente ge = gDAO.getCliente(numDocumentoCliente);

        return ge;
    }

     @Override
    public Proveedor getProveedor(String numDocumentoProveedor) throws SQLException {

        ProveedorDAO pDAO = new ProveedorDAO();

        Proveedor ge = pDAO.getProveedor(numDocumentoProveedor);

        return ge;
    }
    
    
    @Override
    public Empleado getEmpleado(String numDocumentoEmpleado) throws SQLException {

        EmpleadoDAO eDAO = new EmpleadoDAO();

        Empleado ge = eDAO.getEmpleado(numDocumentoEmpleado);

        return ge;
    }

    @Override
    public boolean actualizarGerente(Cliente gerente) throws SQLException {

        return new ClienteDAO().modificar(gerente);
    }

    @Override
    public boolean actualizarEmpleado(Empleado emp) throws SQLException {

        return new EmpleadoDAO().modificar(emp);
    }

    
    @Override
    public boolean cambiarEstadoCliente(String cedula, int estado) throws SQLException {

        Cliente ger = new Cliente();
        ger.setCedula(cedula);
        ger.setEstado(estado);

        return new ClienteDAO().cambiarEstado(ger);
    }
    
    
    @Override
    public boolean confirmarPedido(long codigoP, int estado) throws SQLException {

        Pedido pe = new Pedido();
        pe.setCodigo_pedido(codigoP);
        pe.setEstado(estado);

        return new PedidoDAO().cambiarEstado(pe);
    }

   

    @Override
    public Producto getProducto(long codigo_p) {

        for (Producto producto : this.campañaActiva.getProductos()) {
            if (producto.getCodigo_p() == codigo_p) {
                return producto;
            }
        }
        return null;
    }

    @Override
    public ArrayList<Tipo> getTipos() throws SQLException {
        return new TipoDAO().getTipos();
    }
    
    @Override
    public ArrayList<Empleado> getEmpleados() throws SQLException {
        return new EmpleadoDAO().getEmpleados();
    }
    @Override
    public ArrayList<Especialidad> getEspecialidades() throws SQLException {
        return new EspecialidadDAO().getEspecialidades();
    }

    @Override
    public boolean modificarProducto(Producto p) throws SQLException {

        ProductoDAO pDAO = new ProductoDAO();
        boolean estado = pDAO.modificar(p);

        if (estado) {
            this.campañaActiva.setProductos(new ProductoDAO().listarPorCampaña(campañaActiva));
        }

        return estado;
    }

    @Override
    public boolean insertarImagenDeProducto(ArrayList<String> urls, long codigo_p) throws SQLException {

        ImagenProductoDTO imagen = null;
        ArrayList<ImagenProductoDTO> imagenes = new ArrayList();
        boolean estado = false;
        IDAOImagenProducto iDAO = new ImagenProductoDAO();

        for (int i = 0; i < urls.size(); i++) {
            imagen = new ImagenProductoDTO(urls.get(i));
            imagenes.add(imagen);
        }

        for (int i = 0; i < this.campañaActiva.getProductos().size(); i++) {

            if (this.campañaActiva.getProductos().get(i).getCodigo_p() == codigo_p) {
                this.campañaActiva.getProductos().get(i).getImagenes().addAll(imagenes);
                estado = iDAO.subirImagenes(this.campañaActiva.getProductos().get(i), imagenes);
                break;
            }
        }
        return estado;
    }

   

    
    @Override
    public List<Pedido> getPedidos() throws SQLException, ParseException {
        return new PedidoDAO().listarPorCampaña(this.campañaActiva);
    }

    
    @Override
    public boolean iniciarCampaña(Campaña campaña) throws SQLException, ParseException {

        IDAOCampana campañaDAO = new CampañaDAO();

        boolean estado = campañaDAO.iniciarCampaña(campaña);
       
         if(estado){
            this.campañaActiva = campañaDAO.listarCampañasPorEstado(1).get(0);
       }
     return estado;
    }
    
    @Override
    public boolean iniciarCita(Cita cita) throws SQLException, ParseException {

        IDAOCita citaDAO = new CitaDAO();

        boolean estado = citaDAO.iniciarCita(cita);
       
       
     return estado;
    }
    
    
    @Override
    public boolean subirProductos(ArrayList<Producto> productos) throws SQLException {
        ProductoDAO pDAO = new ProductoDAO();
        boolean estado = pDAO.insertarVarios(productos, campañaActiva);
        this.campañaActiva.setProductos(pDAO.listarPorCampaña(campañaActiva));
        return estado;
    }

  
}
