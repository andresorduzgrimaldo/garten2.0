
package co.edu.ufps.Sigfeja.negocio;
import java.io.Serializable;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import co.edu.ufps.Sigfeja.models.ClasesDAO.ProveedorDAO;
import java.sql.SQLException;

/**
 *
 * @author SANDRA
 */
public class ProveedorNegocio implements Serializable, IProveedorNegocio {
    
    private Proveedor proveedor;

    public ProveedorNegocio() {
    }
    public ProveedorNegocio(String cedula) throws SQLException {
        this.proveedor= new ProveedorDAO().getProveedor(cedula);
    }
    
    @Override
    public Proveedor getProveedor() {
        return proveedor;
    }

    @Override
    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    
    
}
