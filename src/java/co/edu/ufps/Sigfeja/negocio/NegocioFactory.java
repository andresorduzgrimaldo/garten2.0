/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;

/**
 *
 * @author salaas402
 */
public class NegocioFactory implements Serializable{

    public NegocioFactory() {
    }

    public IAdminNegocio getAdminNegocio(Persona p){
        return new AdminNegocio(p);
    }
    
    public IClienteNegocio getClienteNegocio(String cedula) throws SQLException, ParseException{
        return new ClienteNegocio(cedula);
    }
    
    public IGeneralNegocio getGeneralNegocio() throws SQLException, ParseException{
        return new GeneralNegocio();
    }
    
    public IProveedorNegocio getProveedorNegocio(String cedula) throws SQLException{
        return new ProveedorNegocio(cedula);
    }
}
