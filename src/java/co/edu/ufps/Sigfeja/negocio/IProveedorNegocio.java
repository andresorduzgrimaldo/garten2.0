package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;

/**
 *
 * @author SANDRA
 */
public interface IProveedorNegocio {

    public Proveedor getProveedor();

    public void setProveedor(Proveedor proveedor);

}
