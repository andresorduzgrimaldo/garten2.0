/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface IGeneralNegocio {

    public boolean login(Persona p) throws SQLException;

    public Producto getProducto(long codig_p);
    
    public Campaña getCampañaActiva();
    
    public List<Categoria> getCategorias()throws SQLException;
    
    public List<Producto> getProductosCampañaPorCategoriaYTipo(int cat, int tipo) throws SQLException;
    
    public List<Producto> lista(int tipo) throws SQLException;

     public Categoria getCategoria(int id) throws SQLException;

    public ArrayList<Tipo> getTipoProducto() throws SQLException;

    public Tipo getTipo(int id) throws SQLException;
    
    public boolean registrarCliente(Cliente ger) throws SQLException;
    

}
