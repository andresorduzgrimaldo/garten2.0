/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.negocio;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Item;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Pedido;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface IClienteNegocio {

    public Cliente getCliente();

    public void setGerente(Cliente gerente);

    public Producto getProducto(long codigo_p);

    public Categoria getCategoriaC(int id) throws SQLException;

    public List<Pedido> getPedidosC(String cedula) throws SQLException, ParseException;

    public boolean agregarItemAlPedido(Item item);

    public boolean registrarPedido() throws SQLException;

    public boolean eliminarItemDelPedido(int codigo_item);

    public List<Producto> getProductosCampañaPorCategoriaYTipoC(int cat, int tipo) throws SQLException;

    public Campaña getCampañaActiva();

    public boolean existeItem(long codigo_p);

    public boolean actualizarDatos(Cliente ger) throws SQLException;

    public boolean cambiarPassword(String contrasena, String contrasenanueva) throws SQLException;

}
