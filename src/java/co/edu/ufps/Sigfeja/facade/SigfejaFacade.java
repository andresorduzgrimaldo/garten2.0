package co.edu.ufps.Sigfeja.facade;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cita;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Item;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Pedido;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Persona;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import co.edu.ufps.Sigfeja.negocio.IAdminNegocio;
import co.edu.ufps.Sigfeja.negocio.IGeneralNegocio;
import co.edu.ufps.Sigfeja.negocio.IClienteNegocio;
import co.edu.ufps.Sigfeja.negocio.NegocioFactory;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class SigfejaFacade implements Serializable {

    NegocioFactory invocador;
    IAdminNegocio adminN;
    IGeneralNegocio generalN;
    IClienteNegocio clienteN;

    public SigfejaFacade() {
        this.invocador = new NegocioFactory();
    }

    public IAdminNegocio getAdminN() {
        return adminN;
    }

    public void iniciarNegocioAdmin(Persona p) {

        this.adminN = invocador.getAdminNegocio(p);
        this.generalN = null;
        this.clienteN = null;

    }

    public boolean subirProductos(ArrayList<Producto> productos) throws SQLException {
        return this.adminN.subirProductos(productos);
    }
    
    public boolean existeItem(long codigo_p) {
        return this.clienteN.existeItem(codigo_p);
    }

    public List<Producto> getProductosCampañaCategoriaYTipo(int cat, int tipo) throws SQLException {
        return this.generalN.getProductosCampañaPorCategoriaYTipo(cat, tipo);
    }

    public List<Producto> getProductosCampañaCategoriaYTipoC(int cat, int tipo) throws SQLException {
        return this.clienteN.getProductosCampañaPorCategoriaYTipoC(cat, tipo);
    }

    public boolean agregarItemAlPedido(Item item) {
        return this.clienteN.agregarItemAlPedido(item);
    }

    public boolean registrarPedido() throws SQLException {
        return this.clienteN.registrarPedido();
    }
    
    public boolean eliminaItemDelPedido(int codigo_item) {
        return this.clienteN.eliminarItemDelPedido(codigo_item);
    }

    public boolean iniciarCampaña(Campaña cam) throws SQLException, ParseException {
        return this.adminN.iniciarCampaña(cam);
    }

    public boolean iniciarCita(Cita cita) throws SQLException, ParseException {
        return this.adminN.iniciarCita(cita);
    }

    public Campaña getCampañaActiva() {

        if (this.adminN != null) {

            return this.adminN.getCampañaActiva();
        } else if (this.clienteN != null) {
            return this.clienteN.getCampañaActiva();
        } else {

            return this.generalN.getCampañaActiva();
        }
    }

    public List<Categoria> getCategorias() throws SQLException {
        if (this.adminN != null) {
            return this.adminN.getCategorias();
        } else {
            return this.generalN.getCategorias();
        }
    }

    public boolean modificarDatosCliente(String nombre, String apellido, String correo, String direccion, String telefono) throws SQLException {

        Cliente c = this.clienteN.getCliente();
        c.setNombre(nombre);
        c.setApellido(apellido);
        c.setCorreo(correo);
        c.setDireccion(direccion);
        c.setTelefono(telefono);

        if (this.clienteN.actualizarDatos(c)) {
            this.clienteN.setGerente(c);
            return true;
        }

        return false;
    }

    public Categoria getCategoria(int id) throws SQLException {
        return this.generalN.getCategoria(id);
    }

    public Categoria getCategoriaC(int id) throws SQLException {
        return this.clienteN.getCategoriaC(id);
    }

    public boolean modificarProducto(Producto p) throws SQLException {
        return this.adminN.modificarProducto(p);
    }

    public ArrayList<Tipo> getTipos() throws SQLException {
        if (this.adminN != null) {
            return this.adminN.getTipos();
        }

        return null;
    }

    public boolean insertarImagenDeProducto(ArrayList<String> urls, long codigo_p) throws SQLException {
        return this.adminN.insertarImagenDeProducto(urls, codigo_p);
    }

    public List<Pedido> getPedidos() throws SQLException, ParseException {
        return this.adminN.getPedidos();
    }
    public List<Pedido> getPedidosC(String cedula) throws SQLException, ParseException {
        return this.clienteN.getPedidosC(cedula);
    }

    public void iniciarNegocioGeneral() throws SQLException, ParseException {
        this.generalN = invocador.getGeneralNegocio();
        this.adminN = null;
        this.clienteN = null;

    }

    public boolean existeNegocioGeneral() {
        return this.generalN != null;
    }

    public NegocioFactory getInvocador() {
        return invocador;
    }

    public void setInvocador(NegocioFactory invocador) {
        this.invocador = invocador;
    }

    public IGeneralNegocio getGeneralN() {
        return generalN;
    }

    public void setGeneralN(IGeneralNegocio generalN) {
        this.generalN = generalN;
    }

    public IClienteNegocio getClienteN() {
        return clienteN;
    }

    public void setClienteN(IClienteNegocio clienteN) {
        this.clienteN = clienteN;
    }

    public Tipo getTipo(int id) throws SQLException {
        return this.generalN.getTipo(id);
    }

    public void iniciarNegocioCliente(String cedula) throws SQLException, ParseException {

        this.clienteN = invocador.getClienteNegocio(cedula);
        this.generalN = null;
        this.adminN = null;

    }

    public boolean existeNegocioCliente() {
        return this.clienteN != null;
    }

    public Cliente getClienteLogeado() {
        return this.clienteN.getCliente();
    }

    public boolean modificarDatosGerente(String nombre, String apellido, String correo, String direccion, String telefono, String tipoC) throws SQLException {

        Cliente gerente = this.clienteN.getCliente();
        gerente.setNombre(nombre);
        gerente.setApellido(apellido);
        gerente.setCorreo(correo);
        gerente.setDireccion(direccion);
        gerente.setTelefono(telefono);
        gerente.setTipoCli(tipoC);

        if (this.clienteN.actualizarDatos(gerente)) {
            this.clienteN.setGerente(gerente);
            return true;
        }

        return false;
    }

    public boolean cambiarContraseñaCliente(String contrasena, String contrasenanueva) throws SQLException {
        return this.clienteN.cambiarPassword(contrasena, contrasenanueva);
    }

    public Producto getProducto(long codigo_p) {

        if (this.adminN != null) {
            return this.adminN.getProducto(codigo_p);

        } else if (this.clienteN != null) {
            return this.clienteN.getProducto(codigo_p);
        } else {
            return this.generalN.getProducto(codigo_p);
        }
    }

}
