
package co.edu.ufps.Sigfeja.models.ClasesDTO;

import static co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña.formater;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author SANDRA
 */
public class Cita {

    private long id_cita;
    private Empleado cedula_empleado;
    private Calendar fecha_cita;
    private String hora;
    private int estado;

    public Cita() {
    }

    public Cita(Empleado cedula_empleado, Calendar fecha_cita, String hora, int estado) {
        this.cedula_empleado = cedula_empleado;
        this.fecha_cita = fecha_cita;
        this.hora = hora;
        this.estado = estado;
    }

    public Cita(Empleado cedula_empleado, Calendar fecha_cita,String hora) {
        this.cedula_empleado = cedula_empleado;
        this.fecha_cita = fecha_cita;
        this.hora=hora;
    }

    public Cita(long id_cita, Empleado cedula_empleado, Calendar fecha_cita,String hora, int estado) {
        this.id_cita = id_cita;
        this.cedula_empleado = cedula_empleado;
        this.fecha_cita = fecha_cita;
        this.hora=hora;
        this.estado = estado;
    }

    public Calendar getFecha_cita() {
        return fecha_cita;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    
    public void setFecha_cita(Calendar fecha_cita) {
        this.fecha_cita = fecha_cita;
    }

    public String getFechaCitaString() {
        return formater.format(fecha_cita.getTime());
    }

    public long getId_cita() {
        return id_cita;
    }

    public void setId_cita(long id_cita) {
        this.id_cita = id_cita;
    }

    public Empleado getCedula_empleado() {
        return cedula_empleado;
    }

    public void setCedula_empleado(Empleado cedula_empleado) {
        this.cedula_empleado = cedula_empleado;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

}
