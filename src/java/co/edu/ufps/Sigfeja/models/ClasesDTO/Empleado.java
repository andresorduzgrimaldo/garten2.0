/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.models.ClasesDTO;

import java.io.Serializable;

public class Empleado extends Persona implements Serializable {

    private Especialidad cod_especialidad;

    public Empleado() {
    }

    public Empleado(String cedula, String nombre, String Apellido, String correo, String Direccion, String telefono, String contraseña, int tipoUsr, Especialidad cod_especialidad) {
        super(cedula, nombre, Apellido, correo, Direccion, telefono, contraseña, tipoUsr);
        this.cod_especialidad = cod_especialidad;
    }

    public Especialidad getCod_especialidad() {
        return cod_especialidad;
    }

    public void setCod_especialidad(Especialidad cod_especialidad) {
        this.cod_especialidad = cod_especialidad;
    }

    @Override
    public String toString() {
        return "Gerente:" + this.getNombre() + " " + this.getApellido() + "\n Correo " + this.getCorreo()
                + " Dirección" + this.getDireccion() + " Telefono" + this.getTelefono() + " Estado" + this.getEstado();
    }

}
