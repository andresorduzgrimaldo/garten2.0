/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.models.ClasesDTO;

import java.io.Serializable;
import java.util.List;


public class Cliente extends Persona implements Serializable{
    
    private String tipoCli;
    private Pedido pedido;
    public Cliente() {
    }

    public Cliente( String cedula, String nombre, String Apellido, String correo, String Direccion, String telefono, String contraseña, int tipoUsr,String tipoCli) {
        super(cedula, nombre, Apellido, correo, Direccion, telefono, contraseña, tipoUsr);
        this.tipoCli=tipoCli;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    
    
    public String getTipoCli() {
        return tipoCli;
    }

    public void setTipoCli(String tipoCli) {
        this.tipoCli = tipoCli;
    }

    
    @Override
    public String toString() {
        return "Gerente:"+this.getNombre()+" "+this.getApellido()+"\n Correo "+this.getCorreo()
                +" Dirección"+this.getDireccion()+" Telefono"+this.getTelefono()+" Estado"+this.getEstado();
    }
    
}