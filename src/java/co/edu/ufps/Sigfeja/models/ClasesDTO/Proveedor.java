

package co.edu.ufps.Sigfeja.models.ClasesDTO;
import java.io.Serializable;

/**
 *
 * @author SANDRA
 */
public class Proveedor extends Persona implements Serializable{
    
    
    private Tipo tipo;
    
    public Proveedor() {
    }

    public Proveedor( String cedula, String nombre, String Apellido, String correo, String Direccion, String telefono, String contraseña, int tipoUsr,Tipo tipo) {
        super(cedula, nombre, Apellido, correo, Direccion, telefono, contraseña, tipoUsr);
        this.tipo=tipo;
    }

   

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

   @Override
    public String toString() {
        return "Gerente:"+this.getNombre()+" "+this.getApellido()+"\n Correo "+this.getCorreo()
                +" Dirección"+this.getDireccion()+" Telefono"+this.getTelefono()+" Estado"+this.getEstado();
    }
    
    
    
    
    

}
