
package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author SANDRA
 */
public interface IDAOEspecialidad {

    public ArrayList<Especialidad> getEspecialidades() throws SQLException;

    public void closeConn() throws SQLException;

}
