package co.edu.ufps.Sigfeja.models.ClasesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOProveedor;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import co.edu.ufps.Sigfeja.models.util.Conexion;
import co.edu.ufps.Sigfeja.models.util.Encriptador;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SANDRA
 */
public class ProveedorDAO implements Serializable, IDAOProveedor {

    private Conexion con;

    public ProveedorDAO() throws SQLException {
        this.con = new Conexion();
    }

    @Override
    public boolean insertar(Proveedor ger) throws SQLException {
        boolean respuesta = false;

        String sentencia = "INSERT INTO persona VALUES(?,?,?,?,?,?,?,4,1)";
        String sentencia2 = "INSERT INTO proveedor VALUES (?,?)";
        PreparedStatement state = null;
        PreparedStatement state2 = null;

        if (con == null) {
            con = new Conexion();
        }

        try {
            con.getConexion().setAutoCommit(false);

            state = con.getConexion().prepareStatement(sentencia);
            state2 = con.getConexion().prepareStatement(sentencia2);

            state.setString(1, ger.getCedula());
            state.setString(2, ger.getNombre());
            state.setString(3, ger.getApellido());
            state.setString(4, ger.getCorreo());
            state.setString(5, ger.getDireccion());
            state.setString(6, ger.getTelefono());
            state.setString(7, Encriptador.encriptar(ger.getContraseña()));

            state2.setString(1, ger.getCedula());
            state2.setInt(2, ger.getTipo().getId());
            state.execute();
            state2.execute();

            con.getConexion().commit();
            con.getConexion().setAutoCommit(true);
        } catch (SQLException ex) {
            System.out.println("Error Insertando. Revirtiendo Cambios");
            ex.printStackTrace();

            try {
                con.getConexion().rollback();
            } catch (SQLException ex1) {
                System.out.println("Error en el rollback");
                ex1.printStackTrace();
                throw ex1;
            }
            throw ex;
        }
        try {
            state.close();
            state2.close();
            this.closeConn();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }

        return respuesta;
    }

    @Override
    public List<Proveedor> listarPorEstado(int estado) throws SQLException {

        List<Proveedor> pros = new ArrayList();

        String consulta = "SELECT persona.*,tipo_producto.id_tipo_producto,tipo_producto.descripcion AS descripcion FROM persona INNER JOIN proveedor ON "
                + "persona.Cedula=proveedor.cedula INNER JOIN tipo_producto ON proveedor.tipo_producto=tipo_producto.id_tipo_producto WHERE persona.estado=?";

        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setInt(1, estado);
        ResultSet rs = state.executeQuery();
        Proveedor proveedor = null;

        while (rs.next()) {

            proveedor = new Proveedor(rs.getString("Cedula"),
                    rs.getString("Nombre"), rs.getString("Apellido"), rs.getString("Correo"),
                    rs.getString("Direccion"), rs.getString("Telefono"), rs.getString("contrasena"),
                    rs.getInt("TipoUsuario"), null);
            proveedor.setEstado(rs.getInt("estado"));
            Tipo tip = new Tipo(rs.getInt("id_tipo_producto"), rs.getString("descripcion"));
            proveedor.setTipo(tip);
            pros.add(proveedor);

        }

        state.close();
        rs.close();

        con.close();

        return pros;
    }

    @Override
    public Proveedor getProveedor(String cedula) throws SQLException {

        String consulta ="SELECT persona.*,tipo_producto.id_tipo_producto,tipo_producto.descripcion AS descripcion FROM persona INNER JOIN proveedor ON "
                + "persona.Cedula=proveedor.cedula INNER JOIN tipo_producto ON proveedor.tipo_producto=tipo_producto.id_tipo_producto WHERE proveedor.Cedula=?";


        if (con == null) {
            con = new Conexion();
        }
        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setString(1, cedula);

        ResultSet resultado = state.executeQuery();

        Proveedor g = null;

        while (resultado.next()) {

            g = new Proveedor(resultado.getString("Cedula"), resultado.getString("Nombre"),
                    resultado.getString("Apellido"), resultado.getString("Correo"),
                    resultado.getString("Direccion"), resultado.getString("Telefono"),
                    resultado.getString("contrasena"), resultado.getInt("TipoUsuario"),null);
            g.setEstado(resultado.getInt("estado"));
           Tipo tip= new Tipo(resultado.getInt("id_tipo_producto"),resultado.getString("descripcion") );
           g.setTipo(tip);
        }

        state.close();
        resultado.close();

        con.close();

        return g;
    }
    @Override
    public void closeConn() throws SQLException {
        con.close();
        con = null;
    }

}
