/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import java.sql.SQLException;
import java.util.List;


public interface IDAOCliente {

    public boolean insertar(Cliente ger) throws SQLException;

    public boolean modificar(Cliente ger) throws SQLException;

    public boolean cambiarContrasena(Cliente ger) throws SQLException;

    public boolean cambiarEstado(Cliente ger) throws SQLException;

    public List<Cliente> listar() throws SQLException;

    public List<Cliente> listarPorEstado(int estado) throws SQLException;

    public Cliente getCliente(String cedula) throws SQLException;

    public void closeConn() throws SQLException;
}
