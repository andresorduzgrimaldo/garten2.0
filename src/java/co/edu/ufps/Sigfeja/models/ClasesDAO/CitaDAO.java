/*
 * @author Pedro Ruiz, Manuel Osorio, Yermison Chavez, Hender Guarin
 * @version Sisvencat 1.0 * 
 */
package co.edu.ufps.Sigfeja.models.ClasesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOCita;
import static co.edu.ufps.Sigfeja.models.ClasesDTO.Campaña.formater;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cita;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import co.edu.ufps.Sigfeja.models.util.Conexion;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author SANDRA
 */
public class CitaDAO implements Serializable, IDAOCita {

    private Conexion con;
    public static final DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

    public CitaDAO() throws SQLException {
        this.con = new Conexion();
    }

    @Override
    public boolean iniciarCita(Cita cita) throws SQLException {

        String consulta = "INSERT INTO cita (cedula_empleado,fecha_cita,hora,estado)VALUES(?,?,?,1)";
        PreparedStatement state = null;

        try {
            if (con == null) {
                con = new Conexion();
            }
            
         
        
            
            state = con.getConexion().prepareStatement(consulta);
            
               
            
            String fecha_cita = formater.format(cita.getFecha_cita().getTime());
            state.setString(1, cita.getCedula_empleado().getCedula());
            state.setString(2, fecha_cita);
            state.setString(3, cita.getHora());

            state.execute();
        } catch (SQLException e) {
            throw e;
        } finally {
            if (state != null) {
                state.close();
            }
            if (con != null) {
                this.closeConn();
            }
        }

        return true;
    }

    @Override
    public List<Cita> listarCitasPorEstado(int estado) throws SQLException, ParseException {

        List<Cita> citas = new ArrayList();

        String consulta = "SELECT * FROM cita WHERE Estado=?";
        PreparedStatement state = null;
        ResultSet rs = null;

        try {
            if (con == null) {
                con = new Conexion();
            }

            state = con.getConexion().prepareStatement(consulta);
            state.setInt(1, estado);

            rs = state.executeQuery();
            Cita cita = null;

            while (rs.next()) {

                Calendar fechaci = Calendar.getInstance();
                fechaci.setTime(formater.parse(rs.getString("Fecha_cita")));

               
                cita = new Cita(null,fechaci,null);
                Empleado em= new Empleado(rs.getString("cedula_empleado"), null,null, null, null,null, null,1, null);
                cita.setId_cita(rs.getLong("id_cita"));
                cita.setHora(rs.getString("hora"));
                cita.setCedula_empleado(em);
                cita.setEstado(rs.getInt("Estado"));
                

                citas.add(cita);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (state != null) {
                state.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (con != null) {
                this.closeConn();
            }
        }

        return citas;
    }

    @Override
    public void closeConn() throws SQLException {

        con.close();
        con = null;

    }
}
