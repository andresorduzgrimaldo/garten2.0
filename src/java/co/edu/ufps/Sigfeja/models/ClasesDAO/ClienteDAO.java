package co.edu.ufps.Sigfeja.models.ClasesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOCliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.util.Conexion;
import co.edu.ufps.Sigfeja.models.util.Encriptador;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements Serializable, IDAOCliente {

    private Conexion con;

    public ClienteDAO() throws SQLException {
        this.con = new Conexion();
    }

    @Override
    public boolean insertar(Cliente ger) throws SQLException {

        boolean respuesta = false;

        String sentencia = "INSERT INTO persona VALUES(?,?,?,?,?,?,?,2,1)";
        String sentencia2 = "INSERT INTO cliente VALUES (?,?)";
        PreparedStatement state = null;
        PreparedStatement state2 = null;

        if (con == null) {
            con = new Conexion();
        }

        try {
            con.getConexion().setAutoCommit(false);

            state = con.getConexion().prepareStatement(sentencia);
            state2 = con.getConexion().prepareStatement(sentencia2);

            state.setString(1, ger.getCedula());
            state.setString(2, ger.getNombre());
            state.setString(3, ger.getApellido());
            state.setString(4, ger.getCorreo());
            state.setString(5, ger.getDireccion());
            state.setString(6, ger.getTelefono());
            state.setString(7, Encriptador.encriptar(ger.getContraseña()));

            state2.setString(1, ger.getCedula());
            state2.setString(2, ger.getTipoCli());
            state.execute();
            state2.execute();

            con.getConexion().commit();
            con.getConexion().setAutoCommit(true);
        } catch (SQLException ex) {
            System.out.println("Error Insertando. Revirtiendo Cambios");
            ex.printStackTrace();

            try {
                con.getConexion().rollback();
            } catch (SQLException ex1) {
                System.out.println("Error en el rollback");
                ex1.printStackTrace();
                throw ex1;
            }
            throw ex;
        }
        try {
            state.close();
            state2.close();
            this.closeConn();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }

        return respuesta;
    }

    @Override
    public boolean cambiarContrasena(Cliente cli) throws SQLException {

        String consulta = "UPDATE persona SET contrasena=? WHERE Cedula=?";
        PreparedStatement state = null;

        try {

            if (con == null) {
                con = new Conexion();
            }

            state = con.getConexion().prepareStatement(consulta);
            state.setString(1, new Encriptador().encriptar(cli.getContraseña()));
            state.setString(2, cli.getCedula());

            state.execute();
        } catch (SQLException ex) {
            throw ex;
        } finally {
            if (state != null) {
                state.close();
            }
            if (con != null) {
                this.closeConn();
            }
        }

        return true;
    }
    
    @Override
    public boolean modificar(Cliente ger) throws SQLException {

        String consulta = "UPDATE persona SET Nombre=?, Apellido=?, Correo=?, Direccion=?, Telefono=? WHERE Cedula=?";

        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setString(1, ger.getNombre());
        state.setString(2, ger.getApellido());
        state.setString(3, ger.getCorreo());
        state.setString(4, ger.getDireccion());
        state.setString(5, ger.getTelefono());
        state.setString(6, ger.getCedula());
        state.execute();

        state.close();

        this.closeConn();

        return true;
    }

    @Override
    public boolean cambiarEstado(Cliente ger) throws SQLException {

        String consulta = "UPDATE persona set estado=? WHERE Cedula=?";

        if (con == null) {
            con = new Conexion();
        }
        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setInt(1, ger.getEstado());
        state.setString(2, ger.getCedula());

        state.execute();
        state.close();

        this.closeConn();

        return true;
    }

    @Override
    public List<Cliente> listar() throws SQLException {

        String consulta = "SELECT persona.* FROM persona INNER JOIN cliente ON "
                + "persona.Cedula=cliente.Cedula";
        List<Cliente> clientes = new ArrayList();

        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        ResultSet resultado = state.executeQuery();

        while (resultado.next()) {

            Cliente g = new Cliente(resultado.getString("Cedula"), resultado.getString("Nombre"),
                    resultado.getString("Apellido"), resultado.getString("Correo"),
                    resultado.getString("Direccion"), resultado.getString("Telefono"),
                    resultado.getString("Contrasena"), resultado.getInt("TipoUsuario"), null);
            g.setTipoCli(resultado.getString("Tipo"));

            clientes.add(g);
        }

        state.close();
        resultado.close();

        this.closeConn();

        return clientes;
    }

    @Override
    public Cliente getCliente(String cedula) throws SQLException {

        String consulta = "SELECT persona.* FROM persona INNER JOIN cliente ON "
                + "persona.Cedula=cliente.Cedula  WHERE cliente.Cedula=?";

        if (con == null) {
            con = new Conexion();
        }
        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setString(1, cedula);

        ResultSet resultado = state.executeQuery();

        Cliente g = null;

        while (resultado.next()) {

            g = new Cliente(resultado.getString("Cedula"), resultado.getString("Nombre"),
                    resultado.getString("Apellido"), resultado.getString("Correo"),
                    resultado.getString("Direccion"), resultado.getString("Telefono"),
                    resultado.getString("contrasena"), resultado.getInt("TipoUsuario"), null);
            g.setEstado(resultado.getInt("estado"));
            g.setTipoCli("1");

        }

        state.close();
        resultado.close();

        con.close();

        return g;
    }

//    @Override
//    public boolean cambiarContraseña(Cliente ger) throws SQLException {
//
//        String consulta = "UPDATE persona SET contrasena=? WHERE cedula=?";
//
//        if (con == null) {
//            con = new Conexion();
//        }
//
//        PreparedStatement state = con.getConexion().prepareStatement(consulta);
//        state.setString(1, new Encriptador().encriptar(ger.getContraseña()));
//        state.setString(2, ger.getCedula());
//        state.execute();
//
//        state.close();
//
//        con.close();
//
//        return true;
//    }

    @Override
    public List<Cliente> listarPorEstado(int estado) throws SQLException {

        List<Cliente> clientes = new ArrayList();

        String consulta = "SELECT persona.* FROM persona INNER JOIN cliente ON "
                + "persona.Cedula=cliente.Cedula  WHERE persona.estado=?";

        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setInt(1, estado);
        ResultSet rs = state.executeQuery();
        Cliente cliente = null;

        while (rs.next()) {

            cliente = new Cliente(rs.getString("Cedula"),
                    rs.getString("Nombre"), rs.getString("Apellido"), rs.getString("Correo"),
                    rs.getString("Direccion"), rs.getString("Telefono"), rs.getString("contrasena"),
                    rs.getInt("TipoUsuario"), null);
            cliente.setEstado(rs.getInt("estado"));
            cliente.setTipoCli("1");
            clientes.add(cliente);

        }

        state.close();
        rs.close();

        con.close();

        return clientes;
    }

    @Override
    public void closeConn() throws SQLException {
        con.close();
        con = null;
    }
}
