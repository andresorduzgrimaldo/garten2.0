/*
 * @author Pedro Ruiz, Manuel Osorio, Yermison Chavez, Hender Guarin
 * @version Sisvencat 1.0 * 
 */
package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDTO.Cita;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author SANDRA
 */
public interface IDAOCita {

    
    public void closeConn() throws SQLException;

    public boolean iniciarCita(Cita cita) throws SQLException;
    
     public List<Cita> listarCitasPorEstado(int estado)throws SQLException,ParseException ;
   

}
