/*
 * @author Pedro Ruiz, Manuel Osorio, Yermison Chavez, Hender Guarin
 * @version Sisvencat 1.0 * 
 */
package co.edu.ufps.Sigfeja.models.ClasesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOEspecialidad;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import co.edu.ufps.Sigfeja.models.util.Conexion;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author SANDRA
 */
public class EspecialidadDAO implements Serializable, IDAOEspecialidad{
    
    private Conexion con;

    public EspecialidadDAO() {
    }
    
        @Override
    public ArrayList<Especialidad> getEspecialidades() throws SQLException {
        
        String consulta = "SELECT * FROM especialidad";
        ArrayList<Especialidad> esp = new ArrayList();
        PreparedStatement state = null;
        ResultSet rs = null;

        try {
            if (con == null) {
                con = new Conexion();
            }
            state = con.getConexion().prepareStatement(consulta);
            rs = state.executeQuery();
            Especialidad e = null;

            while (rs.next()) {
                e = new Especialidad(rs.getString("cod_especialidad"), rs.getString("descripcion"));
                esp.add(e);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (state != null) {
                state.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (con != null) {
                this.closeConn();
            }
        }
        return esp;
    }

    
      @Override
    public void closeConn() throws SQLException {
        con.close();
        con = null;
    }
}
