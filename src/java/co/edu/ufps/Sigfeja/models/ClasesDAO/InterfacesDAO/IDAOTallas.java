
package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public interface IDAOTallas {
    
    public ArrayList<String> getTallas() throws SQLException;
    
    public void closeConn() throws SQLException;
}
