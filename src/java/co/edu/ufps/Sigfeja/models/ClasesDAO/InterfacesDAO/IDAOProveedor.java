package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import java.sql.SQLException;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import java.util.List;

/**
 *
 * @author SANDRA
 */
public interface IDAOProveedor {

    public boolean insertar(Proveedor pro) throws SQLException;

    public Proveedor getProveedor(String cedula) throws SQLException;

    public void closeConn() throws SQLException;

    public List<Proveedor> listarPorEstado(int estado) throws SQLException;
}
