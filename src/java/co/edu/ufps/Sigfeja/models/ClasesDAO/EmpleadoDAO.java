package co.edu.ufps.Sigfeja.models.ClasesDAO;

import co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO.IDAOEmpleado;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import co.edu.ufps.Sigfeja.models.util.Conexion;
import co.edu.ufps.Sigfeja.models.util.Encriptador;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmpleadoDAO implements Serializable, IDAOEmpleado {

    private Conexion con;

    public EmpleadoDAO() throws SQLException {
        this.con = new Conexion();
    }

     @Override
    public boolean insertarEmp(Empleado ger) throws SQLException{
      boolean respuesta = false;

        String sentencia = "INSERT INTO persona VALUES(?,?,?,?,?,?,?,3,1)";
        String sentencia2 = "INSERT INTO empleado VALUES (?,?)";
        PreparedStatement state = null;
        PreparedStatement state2 = null;

        if (con == null) {
            con = new Conexion();
        }

        try {
            con.getConexion().setAutoCommit(false);

            state = con.getConexion().prepareStatement(sentencia);
            state2 = con.getConexion().prepareStatement(sentencia2);
            
            state.setString(1, ger.getCedula());
            state.setString(2, ger.getNombre());
            state.setString(3, ger.getApellido());
            state.setString(4, ger.getCorreo());
            state.setString(5, ger.getDireccion());
            state.setString(6, ger.getTelefono());
            state.setString(7, Encriptador.encriptar(ger.getContraseña()));

            state2.setString(1, ger.getCedula());
            state2.setString(2, ger.getCod_especialidad().getCod_especialidad());
            
            state.execute();
            state2.execute();

            con.getConexion().commit();
            con.getConexion().setAutoCommit(true);
        } catch (SQLException ex) {
            System.out.println("Error Insertando. Revirtiendo Cambios");
            ex.printStackTrace();
            
            try {
                con.getConexion().rollback();
            } catch (SQLException ex1) {
                System.out.println("Error en el rollback");
                ex1.printStackTrace();
                throw ex1;
            }
            throw ex;
        }
        try {
            state.close();
            state2.close();
            this.closeConn();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw ex;
        }

        return respuesta;
    }
    
    @Override
    public Empleado getEmpleado(String cedula) throws SQLException {

        String consulta ="SELECT persona.*,especialidad.cod_especialidad,especialidad.descripcion AS descripcion FROM persona INNER JOIN empleado ON "
                + "persona.Cedula=empleado.Cedula INNER JOIN especialidad ON empleado.cod_especialidad=especialidad.cod_especialidad WHERE empleado.Cedula=?";


        if (con == null) {
            con = new Conexion();
        }
        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setString(1, cedula);

        ResultSet resultado = state.executeQuery();

        Empleado g = null;

        while (resultado.next()) {

            g = new Empleado(resultado.getString("Cedula"), resultado.getString("Nombre"),
                    resultado.getString("Apellido"), resultado.getString("Correo"),
                    resultado.getString("Direccion"), resultado.getString("Telefono"),
                    resultado.getString("contrasena"), resultado.getInt("TipoUsuario"),null);
            g.setEstado(resultado.getInt("estado"));
            Especialidad esp= new Especialidad(resultado.getString("cod_especialidad"),resultado.getString("descripcion"));
            g.setCod_especialidad(esp);
        }

        state.close();
        resultado.close();

        con.close();

        return g;
    }
    
    @Override
    public boolean modificar(Empleado emp) throws SQLException {

        String consulta = "UPDATE persona SET Nombre=?, Apellido=?, Correo=?, Direccion=?, Telefono=? WHERE Cedula=?";

        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setString(1, emp.getNombre());
        state.setString(2, emp.getApellido());
        state.setString(3, emp.getCorreo());
        state.setString(4, emp.getDireccion());
        state.setString(5, emp.getTelefono());
        state.setString(6, emp.getCedula());
        state.execute();

        state.close();

        this.closeConn();

        return true;
    }

    @Override
    public ArrayList<Empleado> getEmpleados() throws SQLException {
        
        String consulta = "SELECT persona.*,especialidad.cod_especialidad,especialidad.descripcion AS descripcion FROM persona INNER JOIN empleado ON "
                + "persona.Cedula=empleado.Cedula INNER JOIN especialidad ON empleado.cod_especialidad=especialidad.cod_especialidad";

        ArrayList<Empleado> empleados = new ArrayList();
        PreparedStatement state = null;
        ResultSet rs = null;

        try {
            if (con == null) {
                con = new Conexion();
            }
            state = con.getConexion().prepareStatement(consulta);
            rs = state.executeQuery();
            Empleado empl = null;

            while (rs.next()) {
             empl = new Empleado(rs.getString("Cedula"),
                    rs.getString("Nombre"), rs.getString("Apellido"), rs.getString("Correo"),
                    rs.getString("Direccion"), rs.getString("Telefono"), rs.getString("contrasena"),
                    rs.getInt("TipoUsuario"),null);
            Especialidad esp= new Especialidad(rs.getString("cod_especialidad"),rs.getString("descripcion"));
            empl.setCod_especialidad(esp);
            empleados.add(empl);
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (state != null) {
                state.close();
            }
            if (rs != null) {
                rs.close();
            }
            if (con != null) {
                this.closeConn();
            }
        }
        return empleados;
    }

    
    
    @Override
    public List<Empleado> listarPorEstado(int estado) throws SQLException {

        List<Empleado> empleados = new ArrayList();

        String consulta = "SELECT persona.*,especialidad.cod_especialidad,especialidad.descripcion AS descripcion FROM persona INNER JOIN empleado ON "
                + "persona.Cedula=empleado.Cedula INNER JOIN especialidad ON empleado.cod_especialidad=especialidad.cod_especialidad WHERE persona.estado=?";

        
        if (con == null) {
            con = new Conexion();
        }

        PreparedStatement state = con.getConexion().prepareStatement(consulta);
        state.setInt(1, estado);
        ResultSet rs = state.executeQuery();
        Empleado empleado = null;

        while (rs.next()) {
         
            empleado = new Empleado(rs.getString("Cedula"),
                    rs.getString("Nombre"), rs.getString("Apellido"), rs.getString("Correo"),
                    rs.getString("Direccion"), rs.getString("Telefono"), rs.getString("contrasena"),
                    rs.getInt("TipoUsuario"),null);
            empleado.setEstado(rs.getInt("estado"));
            Especialidad esp= new Especialidad(rs.getString("cod_especialidad"),rs.getString("descripcion"));
            empleado.setCod_especialidad(esp);
            empleados.add(empleado);
            
        }

        state.close();
        rs.close();

        con.close();

        return empleados;
    }

    
    @Override
    public void closeConn() throws SQLException {
        con.close();
        con = null;
    }
}
