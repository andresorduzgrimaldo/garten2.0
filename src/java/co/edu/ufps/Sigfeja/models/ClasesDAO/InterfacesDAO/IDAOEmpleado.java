/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.models.ClasesDAO.InterfacesDAO;

import java.sql.SQLException;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import java.util.ArrayList;
import java.util.List;

public interface IDAOEmpleado {

    public boolean insertarEmp(Empleado ger) throws SQLException;
    
    public ArrayList<Empleado> getEmpleados() throws SQLException;

    public Empleado getEmpleado(String cedula) throws SQLException;

    public boolean modificar(Empleado emp) throws SQLException;

    public List<Empleado> listarPorEstado(int estado) throws SQLException;

    public void closeConn() throws SQLException;
}
