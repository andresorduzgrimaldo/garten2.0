
package co.edu.ufps.Sigfeja.controllers;

import co.edu.ufps.Sigfeja.facade.SigfejaFacade;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Administrador;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Categoria;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Especialidad;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Producto;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Tipo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Admin_Controller", urlPatterns = {"/Admin"})
public class Admin_Controller extends HttpServlet {

    SigfejaFacade Fachada;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String metodo = request.getParameter("metodo");
            String respuesta = "";

            Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            switch (metodo) {
                case "modificarDatos":

                    String nombre = request.getParameter("Nombre");
                    String apellido = request.getParameter("Apellido");
                    String direccion = request.getParameter("Direccion");
                    String cedula = request.getParameter("Cedula");
                    String telefono = request.getParameter("Telefono");
                    String correo = request.getParameter("Correo");

                     {
                        try {
                            this.modificarDatos(nombre, apellido, correo, direccion, telefono);
                            respuesta = "Datos Actualizados";
                        } catch (SQLException ex) {
                            respuesta = "Error SQL: " + ex.getMessage();
                            ex.printStackTrace();
                        }
                    }
                    break;

                case "cambiarContraseña":

                    String contrasena = request.getParameter("Contrasena");
                    String contrasenanueva = request.getParameter("ContrasenaNueva");

                     {
                        try {
                            boolean exito = this.cambiarContraseña(contrasena, contrasenanueva);
                            if (exito == true) {
                                respuesta = "Contraseña Actualizada";
                            } else {
                                respuesta = "Error contraseña actual incorrecta";
                            }
                        } catch (SQLException ex) {
                            respuesta = "Error SQL: " + ex.getMessage();
                            ex.printStackTrace();
                        }
                    }
                    break;

                case "registrarProveedor":

                    nombre = request.getParameter("Nombre");
                    apellido = request.getParameter("Apellido");
                    cedula = request.getParameter("Cedula");
                    direccion = request.getParameter("Direccion");
                    telefono = request.getParameter("Telefono");
                    correo = request.getParameter("Correo");
                    contrasena = request.getParameter("Contrasena");
                    int id_tipo_producto = Integer.parseInt(request.getParameter("id_tipo_producto"));

                     {
                        try {
                            this.registrarProveedor(id_tipo_producto, cedula, nombre, apellido, correo, direccion, telefono, contrasena);
                            respuesta = "Proveedor Registrado";
                        } catch (SQLException ex) {
                            respuesta = "Error al registrar" + ex.getErrorCode();
                            ex.printStackTrace();
                        }
                    }
                    break;

                case "getCliente": {
                    try {
                        respuesta = getCliente(request.getParameter("Cedula"));
                    } catch (SQLException ex) {
                        respuesta = "" + ex.getErrorCode();
                        ex.printStackTrace();
                    }
                    break;
                }
                case "getEmpleado": {
                    try {
                        respuesta = getEmpleado(request.getParameter("Cedula"));
                    } catch (SQLException ex) {
                        respuesta = "" + ex.getErrorCode();
                        ex.printStackTrace();
                    }
                    break;
                }

                case "getProveedor": {
                    try {
                        respuesta = getProveedor(request.getParameter("Cedula"));
                    } catch (SQLException ex) {
                        respuesta = "" + ex.getErrorCode();
                        ex.printStackTrace();
                    }
                    break;
                }

                case "modificarCliente": {
                    cedula = request.getParameter("cedula");
                    nombre = request.getParameter("nombre");
                    apellido = request.getParameter("apellido");
                    direccion = request.getParameter("direccion");
                    telefono = request.getParameter("telefono");
                    correo = request.getParameter("correo");

                    try {
                        this.modificarCliente(cedula, nombre, apellido, direccion, telefono, correo);
                        respuesta = "Modificacion Correcta";
                    } catch (SQLException ex) {
                        respuesta = "No se pudo Actualizar. Error SQL : " + ex.getMessage();
                        ex.printStackTrace();
                    }
                    break;
                }

                case "desactivar": {
                    
                    try {
                        this.desactivarCliente(request.getParameter("cedula"));
                        respuesta = "Desactivado con exito";
                    } catch (SQLException ex) {
                        respuesta = "No se pudo desactivar el cliente. Error SQL " + ex.getMessage();
                        ex.printStackTrace();
                    }
                    break;
                }

                case "confirmarPedido": {

                    Long codi = Long.parseLong(request.getParameter("codigoP"));
                    try {
                        this.confirmarPedido(codi);
                        respuesta = "Pedido procesado con exito";
                    } catch (SQLException ex) {
                        respuesta = "No se pudo procesar el pedido. Error SQL " + ex.getMessage();
                        ex.printStackTrace();
                    }
                    break;
                }
                case "reactivar": {
                    try {
                        this.reactivarCliente(request.getParameter("cedula"));
                        respuesta = "Reactivado exitosamente";
                    } catch (SQLException ex) {
                        respuesta = "No se pudo reactivar el cliente. Error SQL " + ex.getMessage();
                        ex.printStackTrace();
                    }
                    break;
                }

                
                case "registrarProducto":

                   String nombreP = request.getParameter("Nombre");
                   String descripcionP=request.getParameter("Descripcion");
                   int valor=Integer.parseInt(request.getParameter("Valor"));
                   int cantidad=Integer.parseInt(request.getParameter("Cantidad"));
                   int tipo=Integer.parseInt(request.getParameter("Tipo"));
                   int cat=Integer.parseInt(request.getParameter("Categoria"));
                   
                   {
                        try {

                            this.registrarProducto(nombreP, descripcionP, valor, cantidad, tipo, cat);
                            respuesta = "Producto Registrado";

                        } catch (SQLException ex) {
                            respuesta = "" + ex.getErrorCode();
                            respuesta = "El Producto Ya Se Encuentra Registrado";
                            ex.printStackTrace();
                        }
                    }
                    break;
                
                case "registrarEmpleado":

                    nombre = request.getParameter("Nombre");
                    apellido = request.getParameter("Apellido");
                    cedula = request.getParameter("Cedula");
                    direccion = request.getParameter("Direccion");
                    telefono = request.getParameter("Telefono");
                    correo = request.getParameter("Correo");
                    contrasena = request.getParameter("Contrasena");
                    String especialidad = request.getParameter("cod_especialidad");
                     {
                        try {

                            this.registrarEmpleado(cedula, nombre, apellido, correo, direccion, telefono, contrasena, especialidad);

                            respuesta = "Empleado Registrado";

                        } catch (SQLException ex) {
                            respuesta = "" + ex.getErrorCode();
                            respuesta = "El Usuario Ya Se Encuentra Registrado";
                            ex.printStackTrace();
                        }
                    }
                    break;

                case "modificarEmpleado": {
                    cedula = request.getParameter("cedula");
                    nombre = request.getParameter("nombre");
                    apellido = request.getParameter("apellido");
                    direccion = request.getParameter("direccion");
                    telefono = request.getParameter("telefono");
                    correo = request.getParameter("correo");

                    try {
                        this.modificarCliente(cedula, nombre, apellido, direccion, telefono, correo);
                        respuesta = "Modificacion Correcta";
                    } catch (SQLException ex) {
                        respuesta = "No se pudo Actualizar. Error SQL : " + ex.getMessage();
                        ex.printStackTrace();
                    }
                    break;
                }
            }
            out.print(respuesta);
        }
    }

    private boolean reactivarCliente(String cedula) throws SQLException {

        return Fachada.getAdminN().cambiarEstadoCliente(cedula, 1);

    }

    private boolean desactivarCliente(String cedula) throws SQLException {

        return Fachada.getAdminN().cambiarEstadoCliente(cedula, 2);

    }

    private boolean confirmarPedido(long codigoP) throws SQLException {
        return Fachada.getAdminN().confirmarPedido(codigoP, 0);

    }

    private boolean modificarCliente(String cedula, String nombre, String apellido, String direccion, String telefono, String correo) throws SQLException {

        Cliente c = new Cliente(cedula, nombre, apellido, correo, direccion, telefono, correo, 2, "1");

        return Fachada.getAdminN().actualizarGerente(c);
    }

    private String getCliente(String cedula) throws SQLException {

        Cliente gerente = Fachada.getAdminN().getCliente(cedula);

        String respuesta = "<h1 class='text-primary text-center'>Modificar Datos del cliente</h1><br>"
                + "<form id=\"formActualizarDatosGerente\">\n"
                + "                                    <h3>Datos Personales</h3>\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"firstname\">Nombres</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"nombre\" id=\"nombre\" value='" + gerente.getNombre() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"lastname\">Apellidos</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"apellido\" id=\"apellido\" value='" + gerente.getApellido() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"street\">Dirección</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"direccion\" id=\"direccion\" value='" + gerente.getDireccion() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"phone\">Teléfono</label>\n"
                + "                                                <input type=\"number\" class=\"form-control\" name=\"telefono\" id=\"telefono\" value='" + gerente.getTelefono() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"email\">Correo</label>\n"
                + "                                                <input type=\"email\" name=\"correo\" id=\"correo\" value='" + gerente.getCorreo() + "' class=\"form-control\" required title=\"Correo No Válido\" />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "<div class='row'>"
                + "<div class='col-sm-6 col-sm-offset-3 text-center'>"
                + "<div class='col-sm-6 text-center'>"
                + "<button name='btnModificarDatosCliente' id='" + gerente.getCedula() + "' onclick='guardarDatosCliente()' type='button' class='btn btn-primary'><i class='fa fa-save'></i> Guardar Cambios</button>"
                + "</div>"
                + "<div class='col-sm-6 text-center'>"
                + "<button class='btnCancelar btn btn-default' type='button' onclick='ocultarDiv(1)'>Cancelar</button>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "                                </form>";

        return respuesta;
    }

    private String getProveedor(String cedula) throws SQLException {

        Proveedor gerente = Fachada.getAdminN().getProveedor(cedula);

        String respuesta = "<h1 class='text-primary text-center'>Modificar Datos del cliente</h1><br>"
                + "<form id=\"formActualizarDatosGerente\">\n"
                + "                                    <h3>Datos Personales</h3>\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"firstname\">Nombres</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"nombre\" id=\"nombre\" value='" + gerente.getNombre() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"lastname\">Apellidos</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"apellido\" id=\"apellido\" value='" + gerente.getApellido() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"street\">Dirección</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"direccion\" id=\"direccion\" value='" + gerente.getDireccion() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"phone\">Teléfono</label>\n"
                + "                                                <input type=\"number\" class=\"form-control\" name=\"telefono\" id=\"telefono\" value='" + gerente.getTelefono() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"email\">Correo</label>\n"
                + "                                                <input type=\"email\" name=\"correo\" id=\"correo\" value='" + gerente.getCorreo() + "' class=\"form-control\" required title=\"Correo No Válido\" />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "<div class='row'>"
                + "<div class='col-sm-6 col-sm-offset-3 text-center'>"
                + "<div class='col-sm-6 text-center'>"
                + "<button name='btnModificarDatosCliente' id='" + gerente.getCedula() + "' onclick='guardarDatosCliente()' type='button' class='btn btn-primary'><i class='fa fa-save'></i> Guardar Cambios</button>"
                + "</div>"
                + "<div class='col-sm-6 text-center'>"
                + "<button class='btnCancelar btn btn-default' type='button' onclick='ocultarDiv(1)'>Cancelar</button>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "                                </form>";

        return respuesta;
    }

    private String getEmpleado(String cedula) throws SQLException {

        Empleado gerente = Fachada.getAdminN().getEmpleado(cedula);

        String respuesta = "<h1 class='text-primary text-center'>Modificar Datos del Empleado</h1><br>"
                + "<form id=\"formActualizarDatosGerente\">\n"
                + "                                    <h3>Datos Personales</h3>\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"firstname\">Nombres</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"nombre\" id=\"nombre\" value='" + gerente.getNombre() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"lastname\">Apellidos</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"apellido\" id=\"apellido\" value='" + gerente.getApellido() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"street\">Dirección</label>\n"
                + "                                                <input type=\"text\" class=\"form-control\" name=\"direccion\" id=\"direccion\" value='" + gerente.getDireccion() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"phone\">Teléfono</label>\n"
                + "                                                <input type=\"number\" class=\"form-control\" name=\"telefono\" id=\"telefono\" value='" + gerente.getTelefono() + "' required />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "                                    <!-- /.row -->\n"
                + "\n"
                + "                                    <div class=\"row\">\n"
                + "\n"
                + "                                        <div class=\"col-sm-6\">\n"
                + "                                            <div class=\"form-group\">\n"
                + "                                                <label for=\"email\">Correo</label>\n"
                + "                                                <input type=\"email\" name=\"correo\" id=\"correo\" value='" + gerente.getCorreo() + "' class=\"form-control\" required title=\"Correo No Válido\" />\n"
                + "                                            </div>\n"
                + "                                        </div>\n"
                + "                                    </div>\n"
                + "<div class='row'>"
                + "<div class='col-sm-6 col-sm-offset-3 text-center'>"
                + "<div class='col-sm-6 text-center'>"
                + "<button name='btnModificarDatosEmpleado' id='" + gerente.getCedula() + "' onclick='guardarDatosEmpleado()' type='button' class='btn btn-primary'><i class='fa fa-save'></i> Guardar Cambios</button>"
                + "</div>"
                + "<div class='col-sm-6 text-center'>"
                + "<button class='btnCancelar btn btn-default' type='button' onclick='ocultarDiv(1)'>Cancelar</button>"
                + "</div>"
                + "</div>"
                + "</div>"
                + "                                </form>";

        return respuesta;
    }

    private boolean modificarEmpleado(String cedula, String nombre, String apellido, String direccion, String telefono, String correo) throws SQLException {

        Empleado e = new Empleado(cedula, nombre, apellido, correo, direccion, telefono, correo, 3, null);

        return Fachada.getAdminN().actualizarEmpleado(e);
    }

    private boolean registrarCliente(String cedula, String nombre, String apellido, String correo, String direccion, String telefono, String contrasena) throws SQLException {

        Cliente ge = new Cliente(cedula, nombre, apellido, correo, direccion, telefono, contrasena, 2, "1");

        return Fachada.getAdminN().registrarCliente(ge);
    }

    private boolean registrarEmpleado(String cedula, String nombre, String apellido, String correo, String direccion, String telefono, String contrasena, String especialidad) throws SQLException {

        Empleado em = new Empleado(cedula, nombre, apellido, correo, direccion, telefono, contrasena, 3, new Especialidad(especialidad, null));

        return Fachada.getAdminN().registrarEmpleado(em);
    }
    
    private boolean registrarProducto(String nombre, String descripcion, int valor, int cantidad, int tipo, int categoria) throws SQLException {

        Producto p= new Producto(0, nombre, descripcion, valor, cantidad,new Categoria(categoria,null), new Tipo(tipo, null), null);
        return Fachada.getAdminN().registrarProducto(p);
    }

    private boolean registrarProveedor(int id_tipo_producto, String cedula, String nombre, String apellido, String correo, String direccion, String telefono, String contrasena) throws SQLException {

        Proveedor pr = new Proveedor(cedula, nombre, apellido, correo, direccion, telefono, contrasena, 4, new Tipo(id_tipo_producto, null));

        return Fachada.getAdminN().registrarProveedor(pr);
    }

    private boolean modificarDatos(String nombre, String apellido, String correo, String direccion, String telefono) throws SQLException {

        Administrador a = new Administrador(null, nombre, apellido, correo, direccion, telefono, null, 1);

        a.setCedula(Fachada.getAdminN().getAdmin().getCedula());
        a.setContraseña(Fachada.getAdminN().getAdmin().getContraseña());

        return Fachada.getAdminN().actualizarDatos(a);
    }

    private boolean cambiarContraseña(String contrasena, String contrasenanueva) throws SQLException {

        return Fachada.getAdminN().cambiarPassword(contrasena, contrasenanueva);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
