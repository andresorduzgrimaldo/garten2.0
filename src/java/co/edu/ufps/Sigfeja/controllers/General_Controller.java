/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ufps.Sigfeja.controllers;

import co.edu.ufps.Sigfeja.facade.SigfejaFacade;
import co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "General_Controller", urlPatterns = {"/General"})
public class General_Controller extends HttpServlet {

    SigfejaFacade Fachada;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String metodo = request.getParameter("metodo");
            String respuesta = "";

            Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            switch (metodo) {

                case "registrarCliente":

                    String nombre = request.getParameter("Nombre");
                    String apellido = request.getParameter("Apellido");
                    String cedula = request.getParameter("Cedula");
                    String direccion = request.getParameter("Direccion");
                    String telefono = request.getParameter("Telefono");
                    String correo = request.getParameter("Correo");
                    String contrasena = request.getParameter("Contrasena");

                     {
                        try {

                            this.registrarCliente(cedula, nombre, apellido, correo, direccion, telefono, contrasena);

                            respuesta = "Cliente Registrado";

                        } catch (SQLException ex) {
                            respuesta = "" + ex.getErrorCode();
                            respuesta = "El Usuario Ya Se Encuentra Registrado";
                            ex.printStackTrace();
                        }
                    }
                    break;
            }
             out.print(respuesta);
        }
           
    }

    private boolean registrarCliente(String cedula, String nombre, String apellido, String correo, String direccion, String telefono, String contrasena) throws SQLException {

        Cliente ge = new Cliente(cedula, nombre, apellido, correo, direccion, telefono, contrasena, 2, "1");

        return Fachada.getGeneralN().registrarCliente(ge);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
