<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand home" href="Profile.jsp" data-animate-hover="bounce">
                <img src="../public/img/log.png" alt="Garten logo" class="hidden-xs">
                <img src="../public/img/log-small.png" alt="Garten logo" class="visible-xs"><span class="sr-only">Sigfeja - Inicio</span>
            </a>
            <div class="navbar-buttons">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Panel de administración</span>
                    <i class="fa fa-align-justify"></i>
                </button>
            </div>
            
            
        </div>

    </div>
    <!-- /.container -->
</div>