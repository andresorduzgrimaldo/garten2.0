

//Métodos Relacionados con la Gestión de Clientes

$('#btnRegistroCliente').on('click', function (e) {
    e.preventDefault();
    registrarCliente();
});

function registrarCliente() {

    var Nombre = $('#Nombre').val();
    var Apellido = $('#Apellido').val();
    var Cedula = $('#Cedula').val();
    var Telefono = $('#Telefono').val();
    var Correo = $('#Correo').val();
    var Direccion = $('#Direccion').val();
    var Contrasena = $('#contrasena').val();
    var Contrasena2 = $('#contrasena2').val();


if(Nombre===""||Apellido===""||Cedula===""||Telefono===""||Correo===""||Direccion===""||Contrasena===""||Contrasena2===""){
    alert("Campos Vacios");
}else{
    if (Contrasena === Contrasena2) {

        $.ajax({
            type: "POST",
            url: "../General",
            data: {metodo: "registrarCliente", Nombre: Nombre, Apellido: Apellido, Cedula: Cedula, Direccion: Direccion,
                Telefono: Telefono, Correo: Correo, Contrasena: Contrasena}
        }).done(function (respuesta) {

            if (respuesta === 1062) {
                respuesta = "El Usuario Ya Se Encuentra Registrado";
                location.reload();
            }
            
           
        if (respuesta === "Cliente Registrado") {
                location.reload();
            }
            
            alert(respuesta);
        });
    } else {
        alert("Las Contraseñas No Coinciden");
    }
}
}

$('#btnPaso').on('click', function (e) {
    e.preventDefault();
    mensaje();
});

function mensaje(){
    alert("Debe iniciar sesión");
    
}


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
