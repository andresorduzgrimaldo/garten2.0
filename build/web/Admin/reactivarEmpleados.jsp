
<%@page import="java.sql.SQLException"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Empleado"%>
<%@page import="java.util.List"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getAdminN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }
            try {

                List<Empleado> empleados = Fachada.getAdminN().getListadoDeEmpleadosPorEstado(2);
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Admininistrador</title>
    </head>
    <body>
        <jsp:include page="../public/includes/admin/header.jsp" />

        <div id="all">

            <div id="content">
                <div class="container">

                    <div class="col-md-12">

                        <ul class="breadcrumb">
                            <li><a href="Profile.jsp">Inicio</a>
                            </li>
                            <li>Clientes</li>
                        </ul>

                    </div>

                    <jsp:include page="../public/includes/admin/panelAdmin.jsp" />
                            <div class="col-md-9">
                                <div id="results" class="box">
                                    <div id="divListado">


                                        <h1 class="text-primary">Listado de Empleados Inactivos</h1>

                                       
                                        </br>
                                        <%
                                            if (empleados.isEmpty()) {
                                        %>
                                        <h4 class="text-center text-info">NO SE ENCUENTRAN EMPLEADOS INACTIVOS</h4>
                                        <%
                                        } else {
                                        %>
                                        

                                        </br>
                                        </br>
                                        <div class="row">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>

                                                        <tr>
                                                            <th class="text-center">Cédula</th>
                                                            <th class="text-center">Nombre</th>  
                                                            <th class="text-center">Apellido</th>
                                                            <th class="text-center">Tipo Cliente</th> 
                                                            <th class="text-center" colspan="1">Acciones</th> 
                                                        </tr>
                                                    </thead>
                                                    <tbody class="buscar">

                                                        <%
                                                            for (Empleado emp : empleados) {

                                                        %>
                                                        <tr>
                                                            <td class="text-center"><%=emp.getCedula()%></td>
                                                            <td class="text-center"><%=emp.getNombre()%></td>  
                                                            <td class="text-center"><%=emp.getApellido()%></td>
                                                            <td class="text-center"><%=emp.getCod_especialidad().getDescripcion()%></td> 
                                                           
                                                            <td class="text-center">
                                                                <button name="btnReactivar" type="submit" id="<%=emp.getCedula()%>" class="btn btn-xs btn-success" data-toggle="tooltip" title="Reactivar"><i class="fa fa-plus"></i></button>
                                                            </td>
                                                        </tr>

                                                        <%                                                        }
                                                        %>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="divModificar" hidden>

                                    </div>
                                    <%
                                        }
                                    %>

                                </div>
                            </div>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->

                <jsp:include page="../public/includes/footerLogin.jsp" />
            </div>
            <!-- /#all -->
            <script src="../public/js/jquery-1.11.0.min.js"></script>
            <script src="../public/js/bootstrap/bootstrap.min.js"></script>
            <script src="../public/js/Administrador.js" type="text/javascript"></script>
        </div>
    </body>
    <%
    } catch (SQLException e) {
    %>
    <%="Error SQL: " + e.getMessage()%>
    <%
            e.printStackTrace();
        }
    %>
</html>
