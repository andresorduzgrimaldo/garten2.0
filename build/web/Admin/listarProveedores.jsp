
<%@page import="java.sql.SQLException"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Proveedor"%>
<%@page import="java.util.List"%>
<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getAdminN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../../cerrarSesion.jsp";
        </script>
        <%
            }
            try {

                List<Proveedor> proveedores = Fachada.getAdminN().getListadoDeProveedoresPorEstado(1);
        %>
        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Admininistracion</title>
    </head>
    <body>
        <jsp:include page="../public/includes/admin/header.jsp" />

        <div id="all">

            <div id="content">
                <div class="container">
                    <jsp:include page="../public/includes/admin/panelAdmin.jsp" />
                    <div class="col-md-9">
                        <div id="results" class="box">
                            <div id="divListado">

                                <h1 class="text-primary" align="center">Listado de proveedores activos</h1>


                                </br>
                                <%
                                    if (proveedores.isEmpty()) {
                                %>
                                <h4 class="text-center text-info">NO HAY PROVEEDORES ACTIVOS</h4>
                                <%
                                } else {
                                %>


                                <br>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>

                                                <tr>
                                                    <th class="text-center">Cédula</th>
                                                    <th class="text-center">Nombre</th>  
                                                    <th class="text-center">Apellido</th>
                                                    <th class="text-center">Tipo Producto</th>

                                                    <th class="text-center" colspan="2">Acciones</th> 
                                                </tr>
                                            </thead>
                                            <tbody class="buscar">

                                                <%
                                                    for (Proveedor proveedor : proveedores) {

                                                %>
                                                <tr>
                                                    <td class="text-center"><%=proveedor.getCedula()%></td>
                                                    <td class="text-center"><%=proveedor.getNombre()%></td>  
                                                    <td class="text-center"><%=proveedor.getApellido()%></td>
                                                    <td class="text-center"><%=proveedor.getTipo().getDescripcion()%></td>

                                                    <td class="text-center">
                                                        <button name="btnActualizarProveedor" type="submit" id="<%=proveedor.getCedula()%>" class="btn btn-xs btn-info" data-toggle="tooltip" title="Modificar"><i class="fa fa-cogs"></i></button>
                                                    </td>
                                                    <td class="text-center">
                                                        <button name="btnDesactivar" type="submit" id="<%=proveedor.getCedula()%>" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Desactivar"><i class="fa fa-trash-o"></i></button>
                                                    </td>
                                                </tr>

                                                <%                                                        }
                                                %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>                        
                            </div>
                            <div id="divModificar" hidden>

                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>


                </div>
                <!-- /.container -->
            </div>
            <!-- /#content -->

            <jsp:include page="../public/includes/footerLogin.jsp" />
        </div>
        <!-- /#all -->
        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/Administrador.js" type="text/javascript"></script>
    </div>
</body>
<%
} catch (SQLException e) {
%>
<%="Error SQL: " + e.getMessage()%>
<%
        e.printStackTrace();
    }
%>
</html>
