


<%@page import="co.edu.ufps.Sigfeja.facade.SigfejaFacade"%>
<%@page import="co.edu.ufps.Sigfeja.models.ClasesDTO.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            SigfejaFacade Fachada = (SigfejaFacade) request.getSession().getAttribute("Fachada");

            if (Fachada == null) {
        %>
        <script>
            alert("Debe Iniciar Sesión");
            location = "../General/login.jsp";
        </script>
        <%
        } else if (Fachada.getClienteN() == null) {
        %>
        <script>
            alert("Acceso solo para el Administrador");
            location = "../cerrarSesion.jsp";
        </script>
        <%
            }
        %>
        <%
            try {

                Cliente cli = Fachada.getClienteN().getCliente();
        %>

        <jsp:include page="../public/includes/importarlibrerias.jsp" />
        <title>Garten</title>
    </head>
    <body>

        <jsp:include page="../public/includes/headerCliente1.jsp" />

        <div class="navbar navbar-default yamm" role="navigation" id="navbar">
            <div class="container">
                <!--/.navbar-header -->

                <jsp:include page="../public/includes/menuCliente.jsp" />
                <!--/.nav-collapse -->

                <!--/.nav-collapse -->

            </div>
            <!-- /.container -->
        </div>

        <div id="all">

            <div id="content">
                <div class="container">
                    <div class="col-md-12">
                        <div id="results" class="box">
                            <h1 align="center">Ayuda con Mi Jardin</h1>
                            <p>Bienvenido al servicio ayuda con mi jardin obtén la mejor asesoria para crear tu propio jardin a tu disponibilidad estan los mejores expertos en la rama y siente como en casa.</p>
                       
                            <h1 align="center">EN DESARROLLO...</h1>
                        </div>
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /#content -->
                
                <jsp:include page="../public/includes/footerCliente.jsp" />
            </div>
            <!-- /#all -->
        </div>

        <script src="../public/js/jquery-1.11.0.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap.min.js"></script>
        <script src="../public/js/jquery.cookie.js"></script>
        <script src="../public/js/waypoints.min.js"></script>
        <script src="../public/js/bootstrap/bootstrap-hover-dropdown.js"></script>
        <script src="../public/js/owl.carousel.min.js"></script>
        <script src="../public/js/front.js"></script>
    </body>
    <%                                            } catch (Exception e) {
        e.printStackTrace();
    %>
    <script>
location = "../General/index.jsp";
    </script>
    <%
        }
    %>
</html>